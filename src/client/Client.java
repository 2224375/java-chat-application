package client;

import client.gui.LoginPage;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;


/**
 * GROUP 3 (CS 222/222L)
 * Prelim Group Project
 * <p>
 * Authors: Abella, Jahn Crystan
 * Antolin, Euan Opiniano
 * Isabelo, Derek Balageo
 * Lo, Justin Louie Kaw
 * Pigoh, Jarod Daniel Pindug
 * Reyes, Hans Lloyd Agustin
 * <p>
 * Objective of the program:
 * The program is used to connect the client to the server
 **/

public class Client {
    public static void main(String[] args) {
        try {
            // for the server
            Socket server = new Socket("localhost", 2001);
            LoginPage loginPage = new LoginPage(server);
           // closeAll(server, strRdr, strWtr);
        } catch (IOException e) {
            System.out.println(e);
        }
    }


    /**
     * For closing the Socket, ObjectInputStream, and ObjectOutputStream
     * @param socket
     * @param strRdr
     * @param strWtr
     */
    public static void closeAll(Socket socket, ObjectInputStream strRdr, ObjectOutputStream strWtr) {
        try {
            if (strRdr != null) {
                strRdr.close();
            }
            if (strWtr != null) {
                strWtr.close();
            }

            if (socket != null) {
                socket.close();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


}//end of class
