package client.gui;


import client.User;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

/**
 * Creates a GUI for the admin using java swing
 */
public class Admin extends JFrame implements ActionListener {

    private Socket server;
    private JFrame frame1;
    private JLabel username;
    private JLabel password;
    private JLabel id;
    private JLabel adminStatus;
    private JTextField usernameText;
    private JTextField passwordText;
    private JTextField idText;
    private JTextField adminStatusText;


    private JTable userTable;
    private JButton banButton;
    private JButton unBanButton;
    private JButton createButton;
    private JButton deleteButton;
    private JButton clearButton;
    private JButton refreshButton;
    private List<User> users;
    private DefaultTableModel tableModel;

    /**
     * Constructor for initializing instance variables
     * @param server
     */
    public Admin(Socket server) {
        this.server = server;
        frame1 = new JFrame();
        frame1.setSize(800, 500);
        frame1.setLayout(null);
        frame1.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

        username = new JLabel("Input username");
        username.setBounds(200, 305, 100, 20);
        frame1.add(username);

        password = new JLabel("Input Password");
        password.setBounds(350, 305, 100, 20);
        frame1.add(password);

        id = new JLabel("Input ID");
        id.setBounds(500, 305, 100, 20);
        frame1.add(id);

        adminStatus = new JLabel("Admin (yes/no)");
        adminStatus.setBounds(650, 305, 100, 20);
        frame1.add(adminStatus);

        usernameText = new JTextField();
        usernameText.setBounds(200, 325, 100, 20);
        frame1.add(usernameText);

        passwordText = new JTextField();
        passwordText.setBounds(350, 325, 100, 20);
        frame1.add(passwordText);

        idText = new JTextField();
        idText.setBounds(500, 325, 100, 20);
        frame1.add(idText);

        adminStatusText = new JTextField();
        adminStatusText.setBounds(650, 325, 100, 20);
        frame1.add(adminStatusText);

        createButton = new JButton("Create");
        createButton.setBounds(200, 375, 150, 60);
        createButton.addActionListener(this);
        frame1.add(createButton);

        deleteButton = new JButton("Delete");
        deleteButton.setBounds(425, 375, 150, 60);
        deleteButton.addActionListener(this);
        frame1.add(deleteButton);

        banButton = new JButton("Ban");
        banButton.setBounds(40, 50, 150, 60);
        banButton.addActionListener(this);
        frame1.add(banButton);

        unBanButton = new JButton("Unban");
        unBanButton.setBounds(40, 175, 150, 60);
        unBanButton.addActionListener(this);
        frame1.add(unBanButton);

        refreshButton = new JButton("refresh");
        refreshButton.setBounds(650, 370, 100, 20);
        refreshButton.addActionListener(this);
        frame1.add(refreshButton);

        clearButton = new JButton("clear");
        clearButton.setBounds(650, 400, 100, 45);
        clearButton.addActionListener(this);
        frame1.add(clearButton);

        users = new ArrayList<>();

        // Creating a table with the column names Username, Id, Admin Status, and Account Status.
        String[] columnNames = {"Username", "Password", "StatusFC", "Account Status", "ID", "Admin Status"};
        tableModel = new DefaultTableModel(columnNames, 0);


        // Read the XML file and adding the data to the table.
        try {
            File inputFile = new File("resources/users/users.xml");
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();
            Document document = builder.parse(inputFile);

            NodeList nodeList = document.getElementsByTagName("user");
            for (int i = 0; i < nodeList.getLength(); i++) {
                Element user = (Element) nodeList.item(i);

                String name = user.getElementsByTagName("name").item(0).getTextContent();
                String password = user.getElementsByTagName("password").item(0).getTextContent();
                String statfc = user.getElementsByTagName("status").item(0).getTextContent();
                String accountStatus = user.getElementsByTagName("accountStatus").item(0).getTextContent();
                int id = Integer.parseInt(user.getElementsByTagName("id").item(0).getTextContent());
                String admin = user.getElementsByTagName("admin").item(0).getTextContent();

                User u = new User(name, password, statfc, accountStatus, id, admin);
                users.add(u);
                tableModel.addRow(new Object[]{name, password, statfc, accountStatus, id, admin});
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        userTable = new JTable(tableModel);
        JScrollPane scrollPane = new JScrollPane(userTable);
        scrollPane.setBounds(200, 50, 550, 260);
        frame1.add(scrollPane);
        frame1.setVisible(true);
    }

    /**
     * Called just after the user presses the createButton, deleteButton, banButton, unbanButton, or the clearButton
     * @param e the event to be processed
     */
    public void actionPerformed(ActionEvent e) {
        // Creating a new user and adding it to the XML file.
        if (e.getSource() == createButton) {
            String username = usernameText.getText();
            String password = passwordText.getText();
            String id = idText.getText();
            String adminStatus = adminStatusText.getText();

            // Check if username and id are not null or empty
            if (username == null || username.trim().isEmpty() || idText == null) {
                JOptionPane.showMessageDialog(null, "Please enter a valid username and id.","ERROR", JOptionPane.ERROR_MESSAGE);
                return;
            }

            int parsedId = Integer.parseInt(id);

            // If the inputted username or id exists, cancel the creation of user and display a message
            if (userExists(username, parsedId)) {
                JOptionPane.showMessageDialog(null, "XML User already exists!","ERROR",
                        JOptionPane.ERROR_MESSAGE);
            } // Create the user if input has not been created yet
            else {
                User newUser = new User(username, password, "offline", "", parsedId, adminStatus);
                users.add(newUser);
                addUserToXML(newUser);
                tableModel.addRow(new Object[]{newUser.getUsername(), newUser.getPassword(), newUser.getUserStatus(),
                        newUser.getAccountStatus(), newUser.getId(), newUser.getAdminStatus()});
                JOptionPane.showMessageDialog(null, "User " + username
                        + " has been added to the XML file.", "Success!",JOptionPane.INFORMATION_MESSAGE);
            }
        } // Deleting the selected row from the table.
        else if (e.getSource() == deleteButton) {
            int selectedRow = userTable.getSelectedRow();
            if (selectedRow != -1) {
                // Getting the user object from the users list at the selected row.
                User user = users.get(selectedRow);
                // Deleting the user from the XML file.
                deleteUserFromXML(user.getId());
                // Removing the user from the users list.
                users.remove(user);
                // Removing the selected row from the table.
                tableModel.removeRow(selectedRow);
                JOptionPane.showMessageDialog(null, "User " + user.getUsername()
                        + " has been deleted.", "Success!",JOptionPane.INFORMATION_MESSAGE);
            }
        } // Banning the selected row from the table.
        else if (e.getSource() == banButton) {
            int selectedRow = userTable.getSelectedRow();
            if (selectedRow != -1) {
                User user = users.get(selectedRow);
                user.ban();
                // Updating the table model with the new status of the user.
                tableModel.setValueAt(user.getAccountStatus(), selectedRow, 3);
                updateAccountStatus(user.getId(), "Banned");
                JOptionPane.showMessageDialog(null, "User " + user.getUsername()
                        + " has been banned.", "Success!",JOptionPane.INFORMATION_MESSAGE);
            }
        } // Unbanning the selected row from the table
        else if (e.getSource() == unBanButton) {
            int selectedRow = userTable.getSelectedRow();
            if (selectedRow != -1) {
                User user = users.get(selectedRow);
                user.unban();
                tableModel.setValueAt(user.getAccountStatus(), selectedRow, 3);
                updateAccountStatus(user.getId(), "Active");
                JOptionPane.showMessageDialog(null, "User " + user.getUsername()
                        + " has been unbanned.", "Success!",JOptionPane.INFORMATION_MESSAGE);
            }

        } // Clearing the text fields.
        else if (e.getSource() == clearButton) {
            usernameText.setText("");
            passwordText.setText("");
            idText.setText("");
            adminStatusText.setText("");
        }
    }

    /**
     * Takes a User object, creates a new XML element for it, and adds it to the XML file
     *
     * @param user The user object that is being added to the XML file.
     */
    private void addUserToXML(User user) {
        try {
            File inputFile = new File("resources/users/users.xml");
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();
            Document doc = builder.parse(inputFile);

            Node root = doc.getFirstChild();
            Element newUser = doc.createElement("user");

            Element name = doc.createElement("name");
            name.appendChild(doc.createTextNode(user.getUsername()));
            newUser.appendChild(name);

            Element password = doc.createElement("password");
            password.appendChild(doc.createTextNode(user.getPassword()));
            newUser.appendChild(password);

            Element userStatus = doc.createElement("status");
            userStatus.appendChild(doc.createTextNode(user.getUserStatus()));
            newUser.appendChild(userStatus);

            Element accountStatus = doc.createElement("accountStatus");
            accountStatus.appendChild(doc.createTextNode(user.getAccountStatus()));
            newUser.appendChild(accountStatus);

            Element id = doc.createElement("id");
            id.appendChild(doc.createTextNode(Integer.toString(user.getId())));
            newUser.appendChild(id);

            Element admin = doc.createElement("admin");
            admin.appendChild(doc.createTextNode(user.getAdminStatus()));
            newUser.appendChild(admin);

            root.appendChild(newUser);

            // Write the updated XML back to the file
            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");

            Node everyNode = doc.getFirstChild();
            trimWhitespace(everyNode);

            DOMSource source = new DOMSource(doc);
            StreamResult result = new StreamResult(new File("resources/users/users.xml"));
            transformer.transform(source, result);

            System.out.println("User " + user.getUsername() + " has been added to the XML file.");

        } catch (ParserConfigurationException | SAXException | IOException | TransformerException e) {
            e.printStackTrace();
        }
    }


    /**
     * Checks if a user with the given username or id already exists in the XML file
     *
     * @param username The username of the user to be created
     * @param id       The id of the user to be created.
     * @return A boolean value.
     */
    private boolean userExists(String username, int id) {
        try {
            File inputFile = new File("resources/users/users.xml");
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();
            Document doc = builder.parse(inputFile);

            NodeList userList = doc.getElementsByTagName("user");

            // Check if a user with the given username or id already exists in the XML file
            for (int i = 0; i < userList.getLength(); i++) {
                Node userNode = userList.item(i);
                if (userNode.getNodeType() == Node.ELEMENT_NODE) {
                    Element userElement = (Element) userNode;
                    String xmlUsername = userElement.getElementsByTagName("name").item(0).getTextContent();
                    int xmlId = Integer.parseInt(userElement.getElementsByTagName("id").item(0).getTextContent());
                    if (xmlUsername.equals(username) || xmlId == id) {
                        return true;
                    }
                }
            }
        } catch (ParserConfigurationException | SAXException | IOException e) {
            e.printStackTrace();
        }

        return false;
    }

    /**
     * Deletes a user from the XML file
     *
     * @param userID The ID of the user to be deleted
     */
    private void deleteUserFromXML(int userID) {
        try {
            // Parse the XML file
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();
            Document doc = builder.parse(new File("resources/users/users.xml"));

            // Find the user element with the given user ID
            NodeList users = doc.getElementsByTagName("user");
            for (int i = 0; i < users.getLength(); i++) {
                Element user = (Element) users.item(i);
                int id = Integer.parseInt(user.getElementsByTagName("id").item(0).getTextContent());
                if (id == userID) {
                    // Remove the user element
                    user.getParentNode().removeChild(user);

                    // Write the updated XML back to the file
                    TransformerFactory tf = TransformerFactory.newInstance();
                    Transformer transformer = tf.newTransformer();
                    transformer.setOutputProperty(OutputKeys.INDENT, "yes");

                    Node everyNode = doc.getFirstChild();
                    trimWhitespace(everyNode);

                    DOMSource source = new DOMSource(doc);
                    StreamResult result = new StreamResult(new File("resources/users/users.xml"));
                    transformer.transform(source, result);

                    System.out.println("User ID " + userID + " has been deleted from the XML file.");
                    break;
                }
            }
        } catch (ParserConfigurationException | SAXException | IOException | TransformerException e) {
            e.printStackTrace();
        }
    }

    /**
     * Updates the accountStatus of a user in the XML file
     *
     * @param userID        The ID of the user to update
     * @param accountStatus The account status of the user.
     */
    private void updateAccountStatus(int userID, String accountStatus) {
        try {
            // Parse the XML file
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();
            Document doc = builder.parse(new File("resources/users/users.xml"));

            // Find the user element with the given user ID
            NodeList users = doc.getElementsByTagName("user");
            for (int i = 0; i < users.getLength(); i++) {
                Element user = (Element) users.item(i);
                int id = Integer.parseInt(user.getElementsByTagName("id").item(0).getTextContent());
                if (id == userID) {
                    // Update the account status of the user
                    user.getElementsByTagName("accountStatus").item(0).setTextContent(accountStatus);

                    // Write the updated XML back to the file
                    TransformerFactory tf = TransformerFactory.newInstance();
                    Transformer transformer = tf.newTransformer();
                    transformer.setOutputProperty(OutputKeys.INDENT, "yes");

                    Node everyNode = doc.getFirstChild();
                    trimWhitespace(everyNode);

                    DOMSource source = new DOMSource(doc);
                    StreamResult result = new StreamResult(new File("resources/users/users.xml"));
                    transformer.transform(source, result);

                    System.out.println("User ID " + userID + " has been updated to " + accountStatus);
                    break;
                }
            }
        } catch (ParserConfigurationException | SAXException | IOException | TransformerException e) {
            e.printStackTrace();
        }
    }

    /**
     * Recursively trims whitespace from all text nodes in a DOM tree
     *
     * @param node The node to start with.
     */
    public static void trimWhitespace(Node node) {
        NodeList children = node.getChildNodes();
        for (int i = 0; i < children.getLength(); ++i) {
            Node child = children.item(i);
            if (child.getNodeType() == Node.TEXT_NODE && child.getTextContent() != null) {
                child.setTextContent(child.getTextContent().trim());
            }
            trimWhitespace(child);
        }
    }
}
