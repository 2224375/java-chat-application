package client.gui;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;

/**
 * Creates a GUI for the login page used by the user
 */
public class LoginPage implements ActionListener {
    JFrame frame = new JFrame();
    JButton loginButton = new JButton("Login");
    JTextField usernameField = new JTextField();
    JPasswordField passwordField = new JPasswordField();
    JLabel usernameLabel = new JLabel("Username");
    JLabel passwordLabel = new JLabel("Password");
    JLabel statusLabel = new JLabel();

    private Socket server;
    private Socket p2p;



    /**
     * Contructor for initializing instance variables
     * @param server
     */
    public LoginPage(Socket server) {
        this.server = server;
        usernameLabel.setBounds(120, 100, 75, 25);
        passwordLabel.setBounds(120, 150, 75, 25);
        statusLabel.setBounds(120, 250, 400, 35);
        statusLabel.setFont(new Font(Font.SANS_SERIF, Font.BOLD, 18));
        usernameField.setBounds(195, 100, 200, 25);
        passwordField.setBounds(195, 150, 200, 25);
        loginButton.setBounds(245, 200, 100, 25);
        loginButton.setFocusable(false);
        loginButton.addActionListener(this);

        frame.add(loginButton);
        frame.add(usernameField);
        frame.add(passwordField);
        frame.add(usernameLabel);
        frame.add(passwordLabel);
        frame.add(statusLabel);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(600, 420);
        frame.getContentPane().setBackground(Color.decode("#b2beb5"));
        frame.setLayout(null);
        frame.setVisible(true);
    }

    /**
     * Called just after the user presses the loginButton
     * @param e the event to be processed
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        try {
            System.out.println("Connecting to server..");
            ObjectOutputStream strWtr = new ObjectOutputStream(server.getOutputStream());
            ObjectInputStream strRdr = new ObjectInputStream(server.getInputStream());
            boolean in;
            boolean adm = false;
            if (e.getSource() == loginButton) {
                String username = usernameField.getText();
                String password = String.valueOf(passwordField.getPassword());
                strWtr.writeObject(username);
                strWtr.writeObject(password);
                in = (boolean) strRdr.readObject();
                //here we get the users server socket
                int id = ((Integer) strRdr.readObject());
                //check if they are a admin
                adm = (boolean) strRdr.readObject();
                //not server socket should only be connected after the
                //login process is finished
                if (in) {
                    if (adm == false){

                        MainPage mainPage = new MainPage(username,id);
                        mainPage.createMainPage();
                        frame.dispose();
                    }else {
                        Admin admin = new Admin(server);
                        frame.dispose();
                    }

                }else {
                    statusLabel.setForeground(Color.red);
                    statusLabel.setText("Username not found or Username and password does not match");
                }
            }
        }catch (Exception a){
            System.out.println(a);
        }

    }

}
