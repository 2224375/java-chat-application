package client.gui;

import java.awt.*;
import java.awt.event.*;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import javax.swing.*;

/**
 * This class is used for displaying the main chat page of the client.
 */
public class MainPage extends JPanel {
    private JTextArea inboundTextArea;
    private JLabel inboundLabel;
    private JButton inboundSendButton;
    private JButton inboundClearButton;
    private JTextField inboundTextField;
    private JTextArea outboundTextArea;
    private JLabel outboundLabel;
    private JTextField outboundTextField;
    private JButton outboundClearButton;
    private JButton outboundSendButton;
    private JList contactsList;
    private JLabel contactsLabel;
    private JButton contactsRefreshButton;
    private JButton groupChatButton;
    private JButton logoutButton;
    private JTextField joinTextField;
    private JButton joinButton;
    private JButton inboundDisconnectButton;
    private JButton outboundDisconnectButton;
    private String name;
    private int id;
    private Socket idHandler = new Socket("localhost",4000);

    private ServerSocket inBoundServerSocket;
    private Socket inBoundSocket;

    private Socket outBoundSocket;

    /**
     * Constructor for initializing instance variables
     */
    public MainPage(String name,int id) throws IOException {
        this.name = name;
        this.inBoundServerSocket = new ServerSocket(id);
        //construct preComponents
        String[] contactsListItems = {"Jahn Crystan Abella", "Euan Antolin", "Derek Isabelo", "Justin Lo", "Jarod Pigoh", "Hans Lloyd Reyes"};
        //construct component
        inboundDisconnectButton = new JButton("Disconnect");
        inboundDisconnectButton.addActionListener(this::clickedInboundButtons);
        outboundDisconnectButton = new JButton("Disconnect");
        outboundDisconnectButton.addActionListener(this::clickedOutboundButtons);
        inboundTextArea = new JTextArea (5, 5);
        inboundTextArea.setEditable(false); //TODO: TextArea works but doesn't have the rectangle box
        inboundLabel = new JLabel ("Inbound");
        inboundSendButton = new JButton ("Send");
        inboundSendButton.addActionListener(this::clickedInboundButtons);
        inboundClearButton = new JButton ("Clear");
        inboundClearButton.addActionListener(this::clickedInboundButtons);
        inboundTextField = new JTextField (5);
        outboundTextArea = new JTextArea (5, 5);
        outboundTextArea.setEditable(false); //TODO: TextArea works but doesn't have the rectangle box
        outboundLabel = new JLabel ("Outbound");
        outboundTextField = new JTextField (5);
        outboundClearButton = new JButton ("Clear");
        outboundClearButton.addActionListener(this::clickedOutboundButtons);
        outboundSendButton = new JButton ("Send");
        outboundSendButton.addActionListener(this::clickedOutboundButtons);
        contactsList = new JList (contactsListItems);
        contactsLabel = new JLabel ("Contacts List" + name);
        contactsRefreshButton = new JButton ("Refresh");
        contactsRefreshButton.addActionListener(this::clickedRefreshButton);
        groupChatButton = new JButton ("Group Chat");
        groupChatButton.addActionListener(this::clickedGroupChatButton);
        logoutButton = new JButton ("Logout");
        logoutButton.addActionListener(this::clickedLogoutButton);
        joinTextField = new JTextField (5);
        joinButton = new JButton("Join");
        joinButton.addActionListener(this::clickedJoinButton);
        inboundClearButton.setFocusable(false);
        inboundDisconnectButton.setFocusable(false);
        inboundSendButton.setFocusable(false);
        outboundClearButton.setFocusable(false);
        outboundDisconnectButton.setFocusable(false);
        outboundSendButton.setFocusable(false);
        contactsRefreshButton.setFocusable(false);
        groupChatButton.setFocusable(false);
        joinButton.setFocusable(false);
        logoutButton.setFocusable(false);

        //adjust size and set layout
        setPreferredSize (new Dimension (843, 494));
        setLayout (null);

        //add components
        add (inboundTextArea);
        add (inboundLabel);
        add (inboundSendButton);
        add (inboundClearButton);
        add (inboundTextField);
        add (outboundTextArea);
        add (outboundLabel);
        add (outboundTextField);
        add (outboundClearButton);
        add (outboundSendButton);
        add (contactsList);
        add (contactsLabel);
        add (contactsRefreshButton);
        add (groupChatButton);
        add (logoutButton);
        add (joinTextField);
        add (joinButton);
        add (inboundDisconnectButton);
        add (outboundDisconnectButton);


        //set component bounds (only needed by Absolute Positioning)
        inboundTextArea.setBounds (15, 30, 245, 320);
        inboundLabel.setBounds (15, 5, 100, 25);
        inboundSendButton.setBounds (160, 355, 100, 35);
        inboundClearButton.setBounds (160, 395, 100, 35);
        inboundDisconnectButton.setBounds(160, 435, 100, 35);
        inboundTextField.setBounds (15, 355, 140, 75);
        outboundTextArea.setBounds (295, 30, 245, 320);
        outboundLabel.setBounds (295, 5, 100, 25);
        outboundTextField.setBounds (295, 355, 140, 75);
        outboundClearButton.setBounds (440, 395, 100, 35);
        outboundSendButton.setBounds (440, 355, 100, 35);
        outboundDisconnectButton.setBounds(440, 435, 100, 35);
        contactsList.setBounds (590, 30, 225, 320);
        contactsLabel.setBounds (590, 5, 100, 25);
        contactsRefreshButton.setBounds (590, 355, 100, 35);
        groupChatButton.setBounds (715, 355, 100, 35);
        logoutButton.setBounds (740, 465, 100, 25);
        joinTextField.setBounds (590, 415, 100, 35);
        joinButton.setBounds(715, 415, 100, 35);
    }




    /**
     * Method for creating the main chat page of the client
     */
    public void createMainPage() throws IOException {
        JFrame frame = new JFrame("Main Page");
        frame.setDefaultCloseOperation (JFrame.EXIT_ON_CLOSE);
        frame.getContentPane().add (new MainPage(name,id));
        Image icon = Toolkit.getDefaultToolkit().getImage("resources/images/orange-slice.png");
        frame.setIconImage(icon);
        frame.pack();
        frame.setVisible (true);
    }

    /**
     * Called just after the user clicks the join button
     * @param e Action performed
     */
    private void clickedJoinButton(ActionEvent e) {
        try {
            if (e.getSource() == joinButton) {
                int id = Integer.parseInt(joinTextField.getText());
                outBoundSocket = new Socket("localhost",id);
                System.out.println("You have joined jarod");
            }
        }catch (Exception a){

        }
    }

    /**
     * Called just after the user clicks the outbound disconnect button
     * @param e Action performed
     */
    private void clickedOutBoundDisconnectButton(ActionEvent e) {
        try {
            if (e.getSource() == outboundDisconnectButton) {
                outBoundSocket.close();
                System.out.println("Out bound chat disconnected");
            }
        }catch (Exception a){

        }
    }

    /**
     * Called just after the user clicks the outbound disconnect button
     * @param e Action performed
     */
    private void clickedInBoundDisconnectButton(ActionEvent e) {
        try {
            if (e.getSource() == inboundDisconnectButton) {
                inBoundSocket.close();
                System.out.println("In bound chat disconnected");
            }
        }catch (Exception a){

        }
    }


    /**
     * Called just after the user clicks the refresh button
     * @param e Action performed
     */
    private void clickedRefreshButton(ActionEvent e)  {
        if (e.getSource() == contactsRefreshButton) {
            try {
                ObjectOutputStream strWtr  = new ObjectOutputStream(idHandler.getOutputStream());;
                ObjectInputStream strRdr   = new ObjectInputStream(idHandler.getInputStream());;
                strWtr.writeObject("giveID");
                String [] nameAndId = (String[]) strRdr.readObject();
                contactsList.setListData(nameAndId);
            }catch (IOException a){

            } catch (ClassNotFoundException ex) {
                throw new RuntimeException(ex);
            }

            //TODO: conference.Contacts refresh button
        }
    }

    /**
     * Called just after the user clicks the logout button.
     * @param e Action performed
     */
    private void clickedLogoutButton(ActionEvent e) {
        if (e.getSource() == logoutButton) {
            //TODO: Logout button
        }
    }

    /**
     * Called just after the user clicks the group chat button
     * @param e Action performed
     */
    private void clickedGroupChatButton(ActionEvent e) {
        //TODO: GROUP CHAT BUTTON
        if (e.getSource() == groupChatButton) {

        }
    }

    /**
     * Called just after the user clicks the send or clear buttons in the inbound section.
     * @param e Action performed
     */
    public void clickedInboundButtons(ActionEvent e) {
        //TODO: Send and clear buttons for inbound
        if (e.getSource() == inboundSendButton) {
            String newLine = inboundTextField.getText();
            String oldLine = inboundTextArea.getText();

            inboundTextArea.setText(oldLine + "\n" + newLine);
            inboundTextField.setText("");
        } else if (e.getSource() == inboundClearButton) {
            inboundTextField.setText("");
            inboundTextArea.setText("");
        }
    }

    /**
     * Called just after the user clicks the send or clear buttons in the outbound section.
     * @param e Action performed
     */
    public void clickedOutboundButtons(ActionEvent e) {
        //TODO: Send and clear buttons for outbound
        if (e.getSource() == outboundSendButton) {
            String newLine = outboundTextField.getText();
            String oldLine = outboundTextArea.getText();

            outboundTextArea.setText(oldLine + "\n" + newLine);
            outboundTextField.setText("");
        } else if (e.getSource() == outboundClearButton) {
            outboundTextArea.setText("");
            outboundTextField.setText("");
        }
    }

    /**
     * Called just after the user clicks the refresh or join button in the contacts section
     * @param e Action performed
     */
    public void clickedContactButtons(ActionEvent e) {
        if (e.getSource() == contactsRefreshButton) {
            //TODO: Refresh list of contacts


        } else if (e.getSource() == joinButton) {
            //TODO: Join button

        }
    }

    /**
     * The sendMessage() sends the name of the user to the server,
     * and then it will send the message that the user types in the console
     */


}
