package client;

/**
 * For storing user account information
 */
public class User {
    private String username;
    private String password;
    private Integer id;
    private boolean banned;
    private String userStatus;
    private String accountStatus;
    private String adminStatus;

    /**
     * Constructor for initializing instance variables
     * @param username Username of the user
     * @param password Password of the user
     * @param userStatus Status of the user
     * @param accountStatus Status of the account of the user
     * @param id ID of the user account
     * @param adminStatus Status of the user if the user is an admin or not
     */
    public User(String username, String password, String userStatus, String accountStatus, Integer id, String adminStatus) {
        this.username = username;
        this.password = password;
        this.userStatus = userStatus;
        this.accountStatus = accountStatus;
        this.adminStatus = adminStatus;
        this.id = id;
        this.banned = false;
    }

    /**
     * Returns the username of the account
     * @return Username of the account
     */
    public String getUsername() {
        return username;
    }

    /**
     * Returns the password of the account
     * @return Password of the account
     */
    public String getPassword() {
        return password;
    }

    /**
     * Returns the status of the user
     * @return Status of the user
     */
    public String getUserStatus() {
        return userStatus;
    }

    /**
     * Returns the status of the account
     * @return Status of the account
     */
    public String getAccountStatus() {
        return banned ? "Banned" : "Active";
    }

    /**
     * Returns the ID of the account
     * @return ID of the account
     */
    public Integer getId() {
        return id;
    }

    /**
     * Returns the status of the account if the account is an admin or not
     * @return Admin status of the account
     */
    public String getAdminStatus() {
        return adminStatus;
    }

    /**
     * To prevent the account from logging in
     */
    public void ban() {
        this.banned = true;
    }

    /**
     * To make the banned user able to log in again
     */
    public void unban() {
        this.banned = false;
    }
}
