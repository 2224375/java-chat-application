package server;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * For checking if another user is logged in to the same account another user is logging in to
 */
public class LoginClientHandler implements Runnable{

    private ObjectOutputStream strWtr;
    private ObjectInputStream strRdr;
    private Socket clientSocket;
    private ProcessXml processXml;
    private String name;
    private boolean in;
    private String accounts = "resources/users/users.xml";

    /**
     * Constructor for the client handler
     * @param clientSocket the socket of the client
     */
    public LoginClientHandler(Socket clientSocket) {
        try {
            this.strRdr = new ObjectInputStream(clientSocket.getInputStream());
            this.strWtr = new ObjectOutputStream(clientSocket.getOutputStream());
            this.clientSocket = clientSocket;
            this.processXml = new ProcessXml();
            this.in = false;
        } catch (IOException e) {
            System.out.println(e);
        }

    }

    /**
     * Checks if the user has inputted the correct username and password from the csv file
     */
    @Override
    public synchronized void run() {
        try {
            boolean login;
            do {
    //            ServerSocket comp = new ServerSocket(1700,1);
                String name = (String) strRdr.readObject();
                String password = (String) strRdr.readObject();
                login = processXml.checkCredentials(accounts, name, password);
                if (login == true) {
                    //uncomment this to set a logged in user online
                    processXml.editStatus(accounts, name, "online");
                    this.name = name;
                    this.in = true;
                    System.out.println("You have successfully logged in.");
                    strWtr.writeObject(login);
                    //send the port of the user
                    strWtr.writeObject(processXml.getId(accounts,name));
                    //send the whether the login is a admin
                    strWtr.writeObject(processXml.getAdmin(accounts,name));


                } else if (login == false) {
                    System.out.println("Another user might be using the account..");
                    strWtr.writeObject(login);
                }
            } while (login != true);



            System.out.println("LGH Thread is stopping");
        } catch (Exception e) {
            System.out.println(e);
        }
    }



    /**
     * Returns the name of the account
     * @return name of the account
     */
    public String getName(){
        return this.name;

    }
}
