package server;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;

public class GetIdHandler implements Runnable {

    private ObjectOutputStream strWtr;
    private ObjectInputStream strRdr;
    private ProcessXml processXml;
    private ServerSocket server = new ServerSocket(4000);

    public GetIdHandler() throws IOException {

    }

    @Override
    public synchronized void run() {
        while (true){
            try {
                Socket clientSocket = server.accept();
                    this.strRdr = new ObjectInputStream(clientSocket.getInputStream());
                    this.strWtr = new ObjectOutputStream(clientSocket.getOutputStream());
                    if (strRdr.readObject().equals("giveID")){
                        strWtr.writeObject(processXml.getNameAndId());
                        clientSocket.close();
                    }
            } catch (IOException e) {
                throw new RuntimeException(e);
            } catch (ClassNotFoundException e) {
                throw new RuntimeException(e);
            }
        }

    }
}
