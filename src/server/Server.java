package server;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;



public class Server implements Runnable{

    private static ServerSocket serverSocket = null;


    public Server(ServerSocket serverSocket) {
        this.serverSocket = serverSocket;
    }

    public static void main(String[] args) {
        try {
            ServerSocket serverSocket = new ServerSocket(2001);
            System.out.println("Server is running");
            Thread acceptClients = new Thread(new Server(serverSocket));
            acceptClients.start();
            System.out.println("There you are");

            GetIdHandler getIdHandler = new GetIdHandler();
            Thread getIdThread = new Thread();
            getIdThread.start();


        } catch (IOException e) {
            System.out.println(e);

        }
    }



    @Override
    public synchronized void run() {
        try {
            Thread acceptThread = new Thread(() -> {
                try {
                    Socket clientSocket;

                    //keeps accepting users to connect to the client
                    while (!this.serverSocket.isClosed()) {
                        clientSocket = serverSocket.accept();
                        System.out.println("Client has connected");
                        LoginClientHandler loginClientHandler = new LoginClientHandler(clientSocket);
                        Thread logInThread = new Thread(loginClientHandler);
                        logInThread.start();
                        logInThread.join();

                    }
                } catch (IOException e) {
                    throw new RuntimeException(e);
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }

            });
            acceptThread.start();
            acceptThread.join();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }


    /**
     * For closing the Socket, ObjectInputStream, and ObjectOutputStream
     * @param socket
     * @param strRdr
     * @param strWtr
     */
    public static void closeAll(Socket socket, ObjectInputStream strRdr, ObjectOutputStream strWtr) {
        try {
            if (strRdr != null) {
                strRdr.close();
            }
            if (strWtr != null) {
                strWtr.close();
            }

            if (socket != null) {
                socket.close();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


}
