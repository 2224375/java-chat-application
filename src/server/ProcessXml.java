package server;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.io.IOException;
import java.util.Arrays;

/**
 * Used for processing data from XML
 */
public class ProcessXml {
    private DocumentBuilderFactory dbFactory;
    private DocumentBuilder dBuilder;
    private Document doc;
    private TransformerFactory transformerFactory;
    private Transformer transformer;
    private DOMSource source;
    private StreamResult result;

    /**
     * Constructor for initializing instance variables
     */
    public ProcessXml() {
        this.dbFactory = dbFactory;
        this.dBuilder = dBuilder;
        this.doc = doc;
        this.transformerFactory = transformerFactory;
        this.transformer = transformer;
        this.source = source;
        this.result = result;
    }


    /**
     * Checks if the user is a admin or not from the xml file
     * @param file filepath to be read
     * @param name name of the user that will be checked
     * @return ID of the admin
     */
    public boolean getAdmin(String file, String name) {
        boolean id = false;
        try {
            File credentials = new File(file);
            dbFactory = DocumentBuilderFactory.newInstance();
            dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(credentials);
            id = false;
            NodeList nList = doc.getElementsByTagName("user");
            for (int i = 0; i < nList.getLength(); i++) {
                boolean n = false;
                Node temp = nList.item(i);
                NodeList childNodes = temp.getChildNodes();
                for (int x = 0; x < childNodes.getLength(); x++) {
                    Node temp2 = childNodes.item(x);
                    //checks the name if it matches
                    if (name.equals(temp2.getTextContent())) {
                        n = true;
                    }
                    //here we get their id
                    if (n == true && temp2.getNodeName().equals("admin")){
                        if (temp2.getTextContent().equals("yes")){
                            id = true;
                        }
                    }

                }
            }
        } catch (Exception e) {
            System.out.println(e);
        }
        return id;
    }

    /**
     * Returns the name and ID of a user
     * @return Name and ID of the user
     */
    public String[] getNameAndId() {
        String [] nameAndId = new String[0];
        try {
            File credentials = new File("resources/users/users.xml");
            dbFactory = DocumentBuilderFactory.newInstance();
            dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(credentials);

            //stores the list of nodes
            NodeList nList = doc.getElementsByTagName("user");
            // System.out.println(nList.getLength());
            nameAndId = new String[nList.getLength()];
            for (int i = 0; i < nList.getLength(); i++) {
                Node temp = nList.item(i);
                NodeList childNodes = temp.getChildNodes();
                String name = null;
                String id = null;
                for (int x = 0; x < childNodes.getLength(); x++) {
                    Node temp2 = childNodes.item(x);
                    //checks the name if it matches
                    if (temp2.getNodeName().equals("name")) {
                        name =temp2.getTextContent();
                    }
                    //here we get their id
                    if (temp2.getNodeName().equals("id")){
                        id =temp2.getTextContent();
                    }
                }
                nameAndId[i] = name + " " + id;
            }
            System.out.println(Arrays.toString(nameAndId));
            return nameAndId;
        } catch (Exception e) {
            System.out.println(e);
        }
        return nameAndId;
    }

    /**
     * Returns the ID of the user
     * @param file filepath to be read
     * @param name name of the user that will be checked
     * @return ID of the user
     */
    public int getId(String file, String name) {
        int id = 0;
        try {
            File credentials = new File(file);
            dbFactory = DocumentBuilderFactory.newInstance();
            dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(credentials);
            id = 0;
            //stores the list of nodes
            NodeList nList = doc.getElementsByTagName("user");
            // System.out.println(nList.getLength());
            for (int i = 0; i < nList.getLength(); i++) {
                boolean n = false;
                Node temp = nList.item(i);
                NodeList childNodes = temp.getChildNodes();
                for (int x = 0; x < childNodes.getLength(); x++) {
                    Node temp2 = childNodes.item(x);
                    //checks the name if it matches
                    if (name.equals(temp2.getTextContent())) {
                        n = true;
                    }
                    //here we get their id
                    if (n == true && temp2.getNodeName().equals("id")){
                        id = Integer.parseInt(temp2.getTextContent());
                    }
                }
            }
            return id;
        } catch (Exception e) {
            System.out.println(e);
        }
        return id;
    }

    /**
     * Edits the status of a user
     * @param accounts filepath of the csv
     * @param name name of the user
     * @param status status to be changed
     */
    public void editStatus(String accounts, String name,String status) {
        try {
            File inputFile = new File(accounts);
            dbFactory = DocumentBuilderFactory.newInstance();
            dBuilder = dbFactory.newDocumentBuilder();
            doc = dBuilder.parse(inputFile);

            //Get the nodes that have the tag students
            //loops through every element that has students as their parent node

            NodeList nList = doc.getElementsByTagName("user");
            for (int i = 0; i < nList.getLength(); i++) {
                boolean n = false;
                Node temp = nList.item(i);
                NodeList childNodes = temp.getChildNodes();
                for (int x = 0; x < temp.getChildNodes().getLength(); x++) {
                    Node temp2 = childNodes.item(x);

                    //checks the name if it matches
                    if (name.equals(temp2.getTextContent())){
                        n = true;
                    }
                    //checks if the nodes name is status
                    if (temp2.getNodeName().equals("status") && n == true){
                        temp2.setTextContent(status);

                    }

                }
            }
            transformerFactory = TransformerFactory.newInstance();
            transformer = transformerFactory.newTransformer();
            source = new DOMSource(doc);
            StreamResult result=new StreamResult(new File(accounts));
            transformer.transform(source, result);

        } catch (Exception e) {

        }
    }

    /**
     *  For checking the creadentials of the user
     * @param file filepath to be read
     * @param name name of the user to be checked
     * @param password password of the user
     * @return
     * @throws IOException
     * @throws SAXException
     * @throws ParserConfigurationException
     */
    public synchronized boolean checkCredentials(String file, String name, String password) throws IOException, SAXException, ParserConfigurationException {
        try {
            File credentials = new File(file);
            dbFactory = DocumentBuilderFactory.newInstance();
            dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(credentials);

            //stores the list of nodes
            NodeList nList = doc.getElementsByTagName("user");
            // System.out.println(nList.getLength());
            for (int i = 0; i < nList.getLength(); i++) {
                boolean n = false, p = false, s = false;
                Node temp = nList.item(i);
                NodeList childNodes = temp.getChildNodes();
                for (int x = 0; x < childNodes.getLength(); x++) {
                    Node temp2 = childNodes.item(x);
                    //                   System.out.println(temp2.getTextContent());
                    //checks the name if it matches
                    if (name.equals(temp2.getTextContent())) {
                        n = true;
                    }
                    //checks the password if it matches
                    if (password.equals(temp2.getTextContent())) {
                        p = true;
                    }
                    //checks the status of the account
                    if (temp2.getTextContent().equals("offline")) {
                        s = true;
                    }
                    //if the name and password matches while the user is online
                    if (p == true && n == true && s == true) {
                        return true;
                    }

                }
            }


        } catch (Exception e) {
            System.out.println(e);
        }
        return false;
    }
}
