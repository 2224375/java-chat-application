package conference2.serverandclient;

import conference2.gui.CreateGUI;
import conference2.gui.MessengerGUI;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;
import java.net.Socket;

/**
 * Contains button handlers for the login and create user page
 */
public class CreateUser extends CreateGUI {


    public CreateUser(Socket socket) {
        super(socket);
    }

    /**
     * Button handler class for loginButton and createButton in the login page of the admin
     */
    public static class ButtonHandler implements ActionListener {
        /**
         * Called when loginButton or createButton is pressed
         * @param e the event to be processed
         */
        public void actionPerformed(ActionEvent e) {
            if (e.getSource() == loginButton) {
                String username = usernameTextBox.getText();
                String password = passwordTextBox.getText();
                if (!isValidUsername(username)) {
                    JOptionPane.showMessageDialog(null, "Username is not yet created.\nPlease proceed to create user first.");
                } else if (!isCorrectPassword(password, username)) {
                    JOptionPane.showMessageDialog(null, "Password might be incorrect.\nPlease try again.");
                } else {
                  new MessengerGUI(socket, username);
                    sendMessageToServer("Login");
                    sendMessageToServer(username);
                    window.setVisible(false);
                }
            }
            if (e.getSource() == createButton) {
                window.setTitle("Create");
                messenger.setVisible(false);
                userLabel.setVisible(false);
                usernameTextBox.setVisible(false);
                passwordTextBox.setVisible(false);
                passLabel.setVisible(false);
                loginButton.setVisible(false);
                createButton.setVisible(false);

                userCreLabel.setVisible(true);
                userCreTextBox.setVisible(true);
                passCreLabel.setVisible(true);
                passCreTextBox.setVisible(true);
                confirmPassLabel.setVisible(true);
                confirmPasswordTextBox.setVisible(true);
                createButton1.setVisible(true);
                cancelButton.setVisible(true);

            }
        }
    }

    /**
     * Button handler for the create user page
     */
    public static class creButtonHandler implements ActionListener {
        /**
         * Called when the cancelButton or createButton1 is pressed
         * @param f the event to be processed
         */
        @Override
        public void actionPerformed(ActionEvent f) {
            if (f.getSource() == cancelButton) {
                window.setTitle("Login");
                messenger.setVisible(true);
                userLabel.setVisible(true);
                usernameTextBox.setVisible(true);
                passwordTextBox.setVisible(true);
                passLabel.setVisible(true);
                loginButton.setVisible(true);
                createButton.setVisible(true);

                userCreLabel.setVisible(false);
                userCreTextBox.setVisible(false);
                passCreLabel.setVisible(false);
                passCreTextBox.setVisible(false);
                confirmPassLabel.setVisible(false);
                confirmPasswordTextBox.setVisible(false);
                createButton1.setVisible(false);
                cancelButton.setVisible(false);
            }

            if (f.getSource() == createButton1) {

                String username = userCreTextBox.getText();
                String password= passCreTextBox.getText();
                String confirmPassword= confirmPasswordTextBox.getText();

                if (userExists(username)) {
                    JOptionPane.showMessageDialog(null, "Username already exists.");
                }

                else if (username.equals("")) {
                    JOptionPane.showMessageDialog(null, "Username field is empty.");
                }

                else if (!password.equals(confirmPassword)) {
                    JOptionPane.showMessageDialog(null, "Passwords does not match.");
                }

                else if (password.equals("") && confirmPassword.equals("")) {
                    JOptionPane.showMessageDialog(null, "Password fields are empty.");
                }

                else {
                    String path = "res/Users/" + username;
                    File newUser = new File(path);
                    newUser.mkdirs();
                    sendMessageToServer(username);
                    File userPass = new File(path + "/Password_" + username + ".txt");
                    File contacts = new File(path + "/Contacts_" + username + ".csv");
                    File privMsg = new File(path + "/Private_Message_" + username + ".txt");
                    try (
                            Writer output = new BufferedWriter(new FileWriter("res/Users.txt" , true));
                            FileWriter userPassWriter = new FileWriter(userPass);
                            FileWriter contactsWriter = new FileWriter(contacts);
                            FileWriter privMsgWriter = new FileWriter(privMsg);
                    )
                    {
                        output.append(username + "\n");
                        userPassWriter.write(password);
                        contactsWriter.write("");
                        privMsgWriter.write("");
                    }
                    catch (Exception ex) {
                        ex.printStackTrace();
                    }
                    window.setVisible(false);
                   new MessengerGUI(socket,username);
                }
            }
        }
    }

    /**
     * Checks if the user already exists upon creation and makes the user
     * choose another username for the creation of the account
     * @param username Username of the account
     * @return
     */
    private static boolean userExists(String username) {
        File resource = new File("res/Users/");
        File[] users = resource.listFiles();
        if (resource.listFiles().length != 0) {
            for (File user : users) {
                if (user.getName().equals(username)) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Checks if the username entered is registered
     * @param username Username of the account
     * @return
     */
    private static boolean isValidUsername(String username) {
        File resource = new File("res/Users/");
        File[] users = resource.listFiles();
        if (resource.listFiles().length != 0) {
            for (File user : users) {
                if (user.getName().equals(username)) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Checks if the password matches the username
     * @param password Password of the account
     * @param username Username of the account
     * @return
     */
    private static boolean isCorrectPassword(String password, String username) {
        String path = "res/Users/" + username + "/Password_" + username + ".txt";
        try (BufferedReader fileReader = new BufferedReader(new FileReader(path))) {
            if (fileReader.readLine().equals(password)) {
                return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    /**
     * Sends the username to the server upon pressing the loginButton
     * @param msg message to be sent to the server
     */
    private static void sendMessageToServer(String msg) {
        try {
            PrintWriter writer = new PrintWriter(socket.getOutputStream(), true);
            writer.println(msg);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        Thread thread = new Thread(new ChatClient());
        thread.start();
    }

}
