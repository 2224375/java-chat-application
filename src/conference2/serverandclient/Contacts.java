package conference2.serverandclient;

import javax.swing.*;
import java.io.*;
import java.net.Socket;

/**
 * Handles the information regarding contacts
 */
public class Contacts {
    private String username;
    private String line;
    private String[] contactsDetails;
    private String[] otherContactsDetails;
    private boolean toList;
    private boolean exists;
    private Socket socket;

    /**
     * Constructor for initializing instance variables
     * @param username username of the account
     * @param socket
     */
    public Contacts(String username, Socket socket) {
        this.username = username;
        this.socket = socket;
    }

    /** reads the users' contacts and updates the contacts **/
    public void addContact(String contactName) {
        String line;
        boolean userFound = false;
        String myUsername = username;
        try (
                BufferedReader bufferedReader = new BufferedReader(new FileReader("res/Users.txt"));
                BufferedReader readContacts = new BufferedReader(new FileReader("res/Users/" + myUsername + "/Contacts_" + myUsername + ".csv"));
                BufferedReader readOtherContacts = new BufferedReader(new FileReader("res/Users/" + contactName + "/Contacts_" + contactName + ".csv"));
                Writer writeMyContacts = new BufferedWriter(new FileWriter("res/Users/" + myUsername + "/Contacts_" + myUsername + ".csv", true));
                Writer updateOtherContacts = new BufferedWriter(new FileWriter("res/Users/" + contactName + "/Contacts_" + contactName + ".csv", true)))
        {
            while ((line = bufferedReader.readLine()) != null) {
                if (contactName.equals(line)) {
                    System.out.println("Username found.");
                    userFound = true;
                }
            }
            if (userFound) {
                String temp;
                String tempContact;
                boolean existsInContacts = false;
                boolean existsInOtherContacts = false;
                while (true) {
                    temp = readContacts.readLine();
                    if (temp == null) {
                        break;
                    }
                    String[] cut = temp.split(",");
                    if (cut[0].equals(contactName)) {
                        existsInContacts = true;
                        break;
                    }
                }
                while (true) {
                    tempContact = readOtherContacts.readLine();
                    if (tempContact == null) {
                        break;
                    }
                    String[] cut = tempContact.split(",");
                    if (cut[0].equals(myUsername)) {
                        existsInOtherContacts = true;
                        break;
                    }
                }
                if (!existsInContacts && !existsInOtherContacts) {
                    writeMyContacts.append(contactName + ",User,Not Favorite" + "\n");
                    updateOtherContacts.append(myUsername + ",User,Not Favorite" + "\n");
                    sendMessageToServer(contactName);
                    toList = true;
                }
                else {
                    exists = true;
                    toList = false;
                }
            }
        } catch (Exception ex) {
            exists = false;
            JOptionPane.showMessageDialog(null, "Username does not exist.");
        }
    }

    /** removes contact in the database **/
    public void removeContact(String contactName) throws IOException {
        BufferedReader readContacts = new BufferedReader(new FileReader("res/Users/" + username + "/Contacts_" + username + ".csv"));
        BufferedReader readOtherContacts = new BufferedReader(new FileReader("res/Users/" + contactName + "/Contacts_" + contactName + ".csv"));
        StringBuilder tempUser = new StringBuilder();
        StringBuilder tempContact = new StringBuilder();
        while ((line = readContacts.readLine()) != null) {
            contactsDetails = line.split(",");
            if(!contactsDetails[0].equals(contactName)){
                tempUser.append(contactsDetails[0]).append(",").append(contactsDetails[1]).append(",").append(contactsDetails[2]).append("\n");
            }
        }
        while ((line = readOtherContacts.readLine()) != null) {
            otherContactsDetails = line.split(",");
            if(!otherContactsDetails[0].equals(username)){
                tempContact.append(otherContactsDetails[0]).append(",").append(otherContactsDetails[1]).append(",").append(otherContactsDetails[2]).append("\n");
            }
        }
        String outputUser = tempUser.toString();
        String outputContact = tempContact.toString();
        FileOutputStream fileOutUser = new FileOutputStream("res/Users/"+ username +"/Contacts_" + username + ".csv");
        FileOutputStream fileOutContact = new FileOutputStream("res/Users/"+ contactName +"/Contacts_" + contactName + ".csv");
        fileOutUser.write(outputUser.getBytes());
        fileOutContact.write(outputContact.getBytes());
        fileOutUser.close();
    }

    public boolean continueToList() {
        return toList;
    }

    public boolean doesExist() {
        return exists;
    }

    /**
     * Sends a message to the server
     * @param message message to be sent
     */
    private void sendMessageToServer(String message) {
        try {
            PrintWriter writer = new PrintWriter(socket.getOutputStream(), true);
            writer.println(message);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}