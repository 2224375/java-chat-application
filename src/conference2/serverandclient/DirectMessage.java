package conference2.serverandclient;

import conference2.gui.DirectMessageGUI;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Handles the private messages that users send to each other
 */
public class DirectMessage extends DirectMessageGUI {

    public DirectMessage(String username, String recipient, Socket socket) {
        super(username, recipient, socket);
    }

    /**
     * Handles actions for sending messages
     */
    public static class ButtonHandler implements ActionListener {
        /**
         * Called when the sendButton is pressed
         * @param e the event to be processed
         */
        public void actionPerformed(ActionEvent e) {
            if (privateMessageTextBox.getText().equals("")) {
                sendButton.disable();
            }
            if (privateMessageTextBox.getText() != null) {
                if (e.getSource() == sendButton) {
                    String msg = privateMessageTextBox.getText();
                    send(msg);
                    privateMessageTextBox.setText("");
                    sendButton.disable();
                }
            }
        }
    }

    /**
     * Displays the history of private messages between users
     */
    public static class MessagesThread extends Thread {
        @Override
        public void run() {
            String line;
            try {
                BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
                PrintWriter writer = new PrintWriter(socket.getOutputStream(), true);
                bufferedReader = reader;
                printWriter = writer;
                while (true) {
                    line = reader.readLine();
                    if (line.equals("Message History")) {
                        String msg = reader.readLine();
                        privateMessageTextArea.append("\n" + msg);
                    } else {
                        System.out.println(line);
                        privateMessageTextArea.append("\n" + line);
                    }
                    privateMessageTextArea.setCaretPosition(privateMessageTextArea.getDocument().getLength());//auto scroll to last message
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }

    /**
     * Sends a private message to a user
     * @param msg message to be sent
     */
    private static void send(String msg) {
        SimpleDateFormat timeStamp = new SimpleDateFormat("EEE| h:mm a");
        try {
            String[] messages = msg.split(" ");
            PrintWriter writer = new PrintWriter(socket.getOutputStream(), true);
            if(messages[0].equals("Private Message")) {
                writer.println("Private Message");
            }
            writer.println(timeStamp.format(new Date()) + "| From " + username + ":\n" + msg);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    /**
     * Sends a message to the server
     * @param message message to be sent
     */
    public static void sendMessageToServer(String message) {
        try {
            PrintWriter writer = new PrintWriter(socket.getOutputStream(), true);
            writer.println(message);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}