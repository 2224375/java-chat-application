package conference2.serverandclient;

import conference2.gui.CreateGUI;

import javax.swing.*;
import java.net.InetAddress;
import java.net.Socket;

/**
 * The program will connect the client to the server
 */

public class ChatClient implements Runnable {


    @Override
    public void run() {
        try
        {
            InetAddress host = InetAddress.getByName("localhost");
            int port = 1100;
            Socket socket = new Socket(host,port);
            ChatClient client = new ChatClient();
            client.login(socket);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * This method will call the log in class
     * @param socket set as an object
     */
    private void login(Socket socket){
        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (Exception e) {
            System.out.println("Failed connection");
        }
        new CreateGUI(socket);
    }
}
