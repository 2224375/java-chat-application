package conference2.serverandclient;

import conference2.gui.GroupGUI;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * The class were utilized for adding, kicking, and creating the group conference
 */
public class GroupConference extends GroupGUI {

    public GroupConference(String groupName, String user, Socket socket) throws IOException {
        super(groupName, user, socket);
    }

    /**
     * The button handler functions as the exit and proceed to next frame method
     * it will perform based on button that the user will use
     * The user may exit or send message to the other users
     */
    public static class ButtonHandler implements ActionListener {

        public void actionPerformed(ActionEvent e) {
            if (messageBox.getText().equals("")) {
                sendButton.disable();
            } else if (messageBox.getText() != null) {
                if (e.getSource() == sendButton) {
                    String msg = messageBox.getText();
                    send(msg);
                    messageBox.setText("");
                    sendButton.disable();
                }
            }
            /**
             * The method were utilized to add members in the group
             * The user may be verified if the user has already been added or not
             * If the username is not in the group conference, the user may join the conference
             * else the username request to join may decline
             */
            if (e.getSource() == addMember) {
                String memberName = JOptionPane.showInputDialog("Enter Member Name: ", "Add to Group");
                System.out.println(memberName);
                GroupContacts contacts = new GroupContacts(user, socket);
                if (memberName == null) {
                    return;
                }
                if (memberName.equals(user)) {
                    JOptionPane.showMessageDialog(null, "The user is already in the Group.");
                    return;
                }
                try {
                    contacts.addMember(memberName);
                    if (contacts.continueToList()) {
                        groupMembers.addElement(memberName);
                    }
                    if(contacts.doesExist()){
                        groupMembers.addElement(memberName);
                    }
                    if (!contacts.doesExist()) {
                        return;
                    }
                    if (!contacts.continueToList()) {
                        JOptionPane.showMessageDialog(null, "Contact already added");
                    }
                } catch (HeadlessException ex) {
                    System.out.println("Error");
                    ex.printStackTrace();
                }
            }
            /**
             * The method were utilized to call the remove button to kick member/s from the conference
             */
            if (e.getSource() == removeButton) {
                GroupContacts groupContacts = new GroupContacts(user, socket);
                try {
                    groupContacts.removeMember(selectedName);
                    groupMembers.removeElement(selectedName);

                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        }
    }

    /**
     * The method were utilized to read the message history and write messages
     * That are sent by the used in the group conference that will later on append in the group conference
     */
    public static class MessagesThread extends Thread {
        @Override
        public void run() {
            String line;
            try {
                BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
                PrintWriter writer = new PrintWriter(socket.getOutputStream(), true);
                bufferedReader = reader;
                System.out.println(reader);
                printWriter = writer;
                while (true) {
                    line = reader.readLine();
                    if(line.equals("Message History")) {
                        String msg = reader.readLine();
                        System.out.println(msg);
                        groupChat.append("\n" + msg);
                        groupChat.setCaretPosition(groupChat.getDocument().getLength());
                    } else {
                        System.out.println("Group Conference: " + line);
                        groupChat.append("\n" + line);
                        groupChat.setCaretPosition(groupChat.getDocument().getLength());
                    }

                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }

    public static MouseListener mouseListenerForGroupMembers = new MouseAdapter() {
        @Override
        public void mousePressed(MouseEvent e) {
            if (e.getClickCount() == 1) {
                selectedName = groupMembersList.getSelectedValue();
            }
        }
    };

    /**
     * The method were utilized to send message to other user
     * @param msg
     */
    private static void send(String msg) {
        SimpleDateFormat timeStamp = new SimpleDateFormat("EEE| h:mm a");
        try {
            String[] arr = msg.split(" ");
            PrintWriter writer = new PrintWriter(socket.getOutputStream(), true);
            if (arr[0].equals("Group Message")) {
                writer.println("Group Message");
                writer.println(timeStamp.format(new Date()) + "| From " + groupName + ":\n" + msg);
            } else {
                writer.println(timeStamp.format(new Date()) + "| From " + groupName + ":\n" + msg);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * The method were used to send a message to the server
     * @param msg
     */
    public static void sendMessageToServer(String msg) {
        try {
            PrintWriter writer = new PrintWriter(socket.getOutputStream(), true);
            writer.println(msg);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
