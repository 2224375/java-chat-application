package conference2.serverandclient;

import java.io.*;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

/**
 * Reads specified files from the resources directory and creates a log in interface for the admin
 */
public class ServerConference extends Thread {

    public static List<ConferenceHandler> onlineUsers = new ArrayList<>();

    //port 1100
    private int port = 1100;

    /**
     * The method were used to update the input and output of the file:
     * Users, Groups, Broadcast_Messages
     * @param args
     * @throws IOException
     */

    public static void main(String[] args) throws IOException {
        File newUserFile = new File("res/Users/");
        newUserFile.mkdirs();
        File newGroupFile = new File("res/Groups/");
        newGroupFile.mkdir();
        File broadcastFile = new File("res/Broadcast_Messages.txt/");
        broadcastFile.createNewFile();
        ServerConference serverConference = new ServerConference();
        serverConference.start();
    }

    /**
     * The method accessed the port and letting the user log in
     * If the username has already been created then it will proceed to log in
     * else the username is not yet create, the data will append in the database
     */
    @Override
    public void run() {
        try {
            InetAddress host = InetAddress.getByName("localhost");
            try (ServerSocket serverSocket = new ServerSocket(port, 50, host)) {
                while (true) {
                    Socket socket = serverSocket.accept();
                    BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
                    String message = bufferedReader.readLine();

                    if (message.equals("Login")) {
                        String username = bufferedReader.readLine();
                        ConferenceHandler client = new ConferenceHandler(socket, username);
                        onlineUsers.add(client);
                        Thread thread = new Thread(client);
                        thread.start();

                    } else {
                        String username = bufferedReader.readLine();
                        String path = "res/Users/" + username;
                        File newUser = new File(path);
                        newUser.mkdirs();
                        File contacts = new File(path + "/Contacts_" + username + ".csv");
                        contacts.createNewFile();
                        Writer writeToUsersList = new BufferedWriter(new FileWriter("res/Users.txt", true));
                        writeToUsersList.append(username + "\n");
                        writeToUsersList.close();
                        ConferenceHandler client = new ConferenceHandler(socket, username);
                        onlineUsers.add(client);
                        Thread thread = new Thread(client);
                        thread.start();
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

