package conference2.serverandclient;

import javax.swing.*;
import java.io.*;
import java.net.Socket;
import java.util.ArrayList;

/**
 * Handles information of group chats
 */
public class GroupContacts {
    private boolean groupFound = false;
    private String groupOwner;
    private String groupName;
    private String groupMember;
    private boolean toList;
    private boolean exists;
    private Socket socket;
    private ArrayList<String> membersArray;
    private String line;
    private String[] groupDetails;
    private String path = "res/Groups/";
    private File details;
    private File messageHistory;

    /**
     * Constructor for initializing instance variables
     * @param groupOwner owner of the group
     * @param socket
     */
    public GroupContacts(String groupOwner, Socket socket) {
        this.groupOwner = groupOwner;
        this.socket = socket;
    }

    /**
     * Creates a new group
     * @param groupName name of the group
     * @throws IOException
     */
    public void addGroup(String groupName) throws IOException {
        try (
                Writer groupWrite = new BufferedWriter(new FileWriter("res/Groups.txt", true))
        ) {
            details = new File(path + groupName + "/Details_" + groupName + ".csv");
            messageHistory = new File (path + groupName + "/Group Message_" + groupName + ".txt");
            File newGroup = new File(path + groupName);
            newGroup.mkdir();
            System.out.println(groupName);
            groupWrite.append(groupName + "\n");
            add(groupName);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Adds a member to the created group
     * @param memberName name of the member to be added
     */
    public void addMember(String memberName) {
        this.groupMember = memberName;
        String line;
        String myUsername = groupMember;
        try (BufferedReader bufferedReader = new BufferedReader(new FileReader("res/Users.txt"));
             BufferedReader readContacts = new BufferedReader(new FileReader("res/Users/" + myUsername + "/Contacts_" + myUsername + ".csv"));
             BufferedReader readOtherContacts = new BufferedReader(new FileReader("res/Users/" + memberName + "/Contacts_" + memberName + ".csv"))
        ) {
            while ((line = bufferedReader.readLine()) != null) {
                if (memberName.equals(line)) {
                    System.out.println("Username found.");
                    groupFound = true;
                }
            }
            if (groupFound) {
                String temp;
                String tempContact;
                boolean existsInContacts = false;
                boolean existsInOtherContacts = false;
                while (true) {
                    temp = readContacts.readLine();
                    if (temp == null) {
                        break;
                    }
                    String[] cut = temp.split(",");
                    if (cut[0].equals(memberName)) {
                        existsInContacts = true;
                        break;
                    }
                }
                while (true) {
                    tempContact = readOtherContacts.readLine();
                    if (tempContact == null) {
                        break;
                    }
                    String[] cut = tempContact.split(",");
                    if (cut[0].equals(myUsername)) {
                        existsInOtherContacts = true;
                        break;
                    }
                }
                if (!existsInContacts && !existsInOtherContacts) {
                    sendMessageToServer(memberName);
                    toList = true;
                }
                else {
                    exists = true;
                    toList = false;
                }
            }
        } catch (Exception ex) {
            exists = false;
            JOptionPane.showMessageDialog(null, "Username does not exist.");
        }
    }

    public void removeContact(String groupName) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new FileReader("res/Groups.txt"));
        while ((line = bufferedReader.readLine()) != null) {
            if (line.equals(groupName)) {

            }
        }
    }

    /**
     * Removes a member from the selected group
     * @param selectedName the member to be removed
     * @throws IOException
     */
    public void removeMember(String selectedName) throws IOException {
        BufferedReader readContacts = new BufferedReader(new FileReader("res/Users/" + groupMember + "/Contacts_" + groupMember + ".csv"));
        BufferedReader readOtherContacts = new BufferedReader(new FileReader("res/Users/" + selectedName + "/Contacts_" + selectedName + ".csv"));
        StringBuilder tempUser = new StringBuilder();
        StringBuilder tempContact = new StringBuilder();
        while ((line = readContacts.readLine()) != null) {
            groupDetails = line.split(",");
            if(!groupDetails[0].equals(selectedName)){
                tempUser.append(groupDetails[0] + "," + groupDetails[1] + "," + groupDetails[2] + "\n");
            }
        }
        String outputUser = tempUser.toString();
        String outputContact = tempContact.toString();
        FileOutputStream fileOutUser = new FileOutputStream("res/Users/"+ groupMember +"/Contacts_" + groupMember + ".csv");
        FileOutputStream fileOutContact = new FileOutputStream("res/Users/"+ selectedName +"/Contacts_" + selectedName + ".csv");
        fileOutUser.write(outputUser.getBytes());
        fileOutContact.write(outputContact.getBytes());
        fileOutUser.close();

    }

    /**
     * Creates a new group
     * @param groupName name of the group to be created
     */
    private void add(String groupName) {
        System.out.println(details);
        try (
                BufferedReader bufferedReader = new BufferedReader(new FileReader("res/Groups.txt"));
                Writer writeGroupDetails = new BufferedWriter(new FileWriter(details, true));
                Writer writeMessage = new BufferedWriter(new FileWriter(messageHistory, true))
        ) {

            while ((line = bufferedReader.readLine()) != null) {
                System.out.println("Groups: " + line);
                this.groupDetails = line.split(",");
                if (groupDetails[0].equals(groupName)) {
                    System.out.println("Group name found.");
                    groupFound = true;
                    exists = true;
                } else {
                    groupFound = false;
                    exists = false;
                }
            }
            if (!groupFound) {
                String temp;
                boolean exist = false;
                while (true) {
                    temp = bufferedReader.readLine();
                    System.out.println(temp);
                    if (temp == null) {
                        break;
                    }
                    String[] cut = temp.split(",");
                    if (cut[0].equals(groupName)) {
                        System.out.println("Group Created.");
                        exist = true;
                        break;
                    }
                }
                if (!exist) {
                    System.out.println("Owner:" + groupOwner);
                    writeGroupDetails.append(groupOwner + ",Owner" + "\n");
                    writeMessage.append(groupName + " Group\n");
                    /*for (String groupMember : membersArray) {
                        System.out.println("Member: " + groupMember);
                        writeGroupDetails.append(groupMember + ",Member" + "\n");
                        toList = true;
                    }*/
                    toList = true;
                    exists = true;
                } else {
                    exists = true;
                    toList = false;
                }
            } else if (groupFound) {
                JOptionPane.showMessageDialog(null, "Group Already Exists");
            }
        } catch (IOException e) {
            exists = false;
            JOptionPane.showMessageDialog(null, "Error");
        }
    }

    public boolean continueToList() {
        return toList;
    }

    public boolean doesExist() {
        return exists;
    }

    /**
     * Returns the group name
     * @return group name
     */
    public String getGroupName() { return this.groupName; }

    /**
     * Sends a message to the server
     * @param message message to be sent
     */
    private void sendMessageToServer(String message) {
        try {
            PrintWriter writer = new PrintWriter(socket.getOutputStream(), true);
            writer.println(message);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
