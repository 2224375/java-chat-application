package conference2.serverandclient;


import conference2.gui.Broadcast;
import conference2.gui.DirectMessageGUI;
import conference2.gui.GroupGUI;
import conference2.gui.MessengerGUI;

import javax.swing.*;
import java.awt.event.*;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.net.Socket;

public class Messenger extends MessengerGUI {


    public Messenger(Socket socket, String username) {
        super(socket, username);
    }

    public static class ButtonHandler implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            if (e.getSource() == broadcast) {
                try {
                    new Broadcast(username, socket);
                } catch (Exception ex) {
                    JOptionPane.showMessageDialog(null, "Unable to connect to server.");
                }
            }

            if (e.getSource() == addContact) {
                String contactName = JOptionPane.showInputDialog(null, "Enter the username: ", "Add to Contacts");
                Contacts contacts = new Contacts(username, socket);
                if (contactName == null) {
                    return;
                }
                if (contactName.equals(username)) {
                    JOptionPane.showMessageDialog(null, "You cannot add yourself as a contact.");
                    return;
                }
                try {
                    contacts.addContact(contactName);
                    if (contacts.continueToList()) {
                        contactList.addElement(contactName);
                    }
                    if (contacts.doesExist()) {
                        contactList.addElement(contactName);
                    }
                    if (!contacts.doesExist()) {
                        return;
                    }
                    if (!contacts.continueToList()) {
                        JOptionPane.showMessageDialog(null, "Contact already added.");

                    }
                }catch(Exception exception){
                    exception.printStackTrace();

                }
            }

            if (e.getSource() == addGroup) {
                groupName = JOptionPane.showInputDialog(null, "Enter Group Name: ", "Add Group");
                GroupContacts newGroup  = new GroupContacts(username, socket);
                if (groupName == null) {
                    return;
                }

                try {
                    newGroup.addGroup(groupName);
                    String path = "res/Groups/" + groupName;
                    if (newGroup.continueToList()) {
                        groupList.addElement(groupName);
                        File newGrp = new File(path);

                    }
                    if (newGroup.doesExist()) {
                        groupList.addElement(groupName);
                    }
                    if (!newGroup.doesExist()) {
                        JOptionPane.showMessageDialog(null, "Group Name Taken!");
                    }
                    if (!newGroup.continueToList()) {
                        JOptionPane.showMessageDialog(null, "Group already added!");
                    }

                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }

            if (e.getSource() == addBookmark) {
                Bookmark bookmark = new Bookmark(username);
                String contactName = JOptionPane.showInputDialog(null, "Enter the username: ", "Contact Username");
                if (contactName == null) {
                    return;
                }
                if (contactName.equals(username)) {
                    JOptionPane.showMessageDialog(null, "Invalid bookmark.");
                    return;
                }
                try {
                    bookmark.addToBookmark(contactName);
                    if (bookmark.continueToList()) {
                        bookmarklist.addElement(contactName);
                    }
                    if (!bookmark.doesExist()) {
                        return;
                    }
                    if (!bookmark.continueToList()) {
                        JOptionPane.showMessageDialog(null, "Contact has already been bookmarked.");
                    }
                } catch (Exception exception) {
                    exception.printStackTrace();
                }
            }


            if (e.getSource() == showContacts) {
                groupsLabel.setVisible(false);
                groupsList.setVisible(false);
                groupsListScroller.setVisible(false);
                addGroup.setVisible(false);
                showGroups.setVisible(true);
                removeGroup.setVisible(false);

                bookmarkLabel.setVisible(false);
                bookmarkList.setVisible(false);
                bookmarkListScoller.setVisible(false);
                addBookmark.setVisible(false);
                removeBookmark.setVisible(false);

                contactsLabel.setVisible(true);
                contactsList.setVisible(true);
                contactsListScroller.setVisible(true);
                addContact.setVisible(true);
                showContacts.setVisible(false);
                removeContact.setVisible(true);
            }

            if (e.getSource() == searchContactOrGroup) {
                String contactName = JOptionPane.showInputDialog(null, "Search for a contact: ", "");
                String line;
                String lineG = null;
                boolean userFound = false;
                boolean groupFound = false;
                try{
                    BufferedReader readContact = new BufferedReader(new FileReader("res/Users/" + username + "/Contacts_" + username + ".csv"));
                    BufferedReader readGroup = new BufferedReader(new FileReader("res/Groups/" + username + "/Details_" + username + ".txt"));
                    if (contactName.equals(username)) {
                        JOptionPane.showMessageDialog(null, "You cannot message yourself.");
                        return;
                    }

                    while ((line = readContact.readLine()) != null || (lineG = readGroup.readLine()) != null) {
                        String cutG = lineG;
                        String[] cut = line.split(",");
                        String[] recipient = cut;
                        String nameGroup = cutG;
                        if (contactName.equals(cut[0]) && cut[1].equals("User")) {
                            directMessageGUI = new DirectMessageGUI(username, recipient[0], socket);
                            userFound = true;
                        }
                        if (contactName.equals(cutG)) {
                            groupGUI = new GroupGUI(nameGroup, username, socket);
                            groupFound = true;
                        }
                    }
                    if (!userFound || !groupFound) {
                        JOptionPane.showMessageDialog(null, "Contact does not exist.");
                    }
                } catch (Exception ex) {
                }
            }

            if (e.getSource() == showBookmark) {
                if (showGroups.isVisible()) {
                    showGroups.setVisible(false);
                    showContacts.setVisible(true);
                } else {
                    showContacts.setVisible(false);
                    showGroups.setVisible(true);
                }

                groupsLabel.setVisible(false);
                groupsList.setVisible(false);
                groupsListScroller.setVisible(false);
                addGroup.setVisible(false);
                removeGroup.setVisible(false);

                contactsLabel.setVisible(false);
                contactsList.setVisible(false);
                contactsListScroller.setVisible(false);
                addContact.setVisible(false);
                removeContact.setVisible(false);

                bookmarkLabel.setVisible(true);
                bookmarkList.setVisible(true);
                bookmarkListScoller.setVisible(true);
                addBookmark.setVisible(true);
                removeBookmark.setVisible(true);
            }

            if (e.getSource() == showGroups) {
                contactsLabel.setVisible(false);
                contactsList.setVisible(false);
                contactsListScroller.setVisible(false);
                addContact.setVisible(false);
                showContacts.setVisible(true);
                removeContact.setVisible(false);

                bookmarkLabel.setVisible(false);
                bookmarkList.setVisible(false);
                bookmarkListScoller.setVisible(false);
                addBookmark.setVisible(false);
                removeBookmark.setVisible(false);

                groupsLabel.setVisible(true);
                groupsList.setVisible(true);
                groupsListScroller.setVisible(true);
                addGroup.setVisible(true);
                showGroups.setVisible(false);
                removeGroup.setVisible(true);
            }

            if (e.getSource() == removeContact) {
                Contacts contacts = new Contacts(username, socket);
                try {
                    contacts.removeContact(selectedName);
                    contactList.removeElement(selectedName);
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }

            if (e.getSource() == removeGroup) {
                GroupContacts groupContacts = new GroupContacts(username, socket);
                try {
                    groupContacts.removeContact(selectedGroup);
                    groupList.removeElement(selectedGroup);
                } catch (Exception ex) {
                    ex.printStackTrace();
                }

            }

            if (e.getSource() == removeBookmark) {
                Bookmark bookmark = new Bookmark(username);
                try {
                    bookmark.removeBookmark(selectedName);
                    bookmarklist.removeElement(selectedName);
                } catch (IOException ioException) {
                    ioException.printStackTrace();
                }
            }
        }
    }

    /**
     * when user selects a contact from the list of contacts the client calls the PrivateMessage class
     **/
    public static MouseListener mouseListenerForContacts = new MouseAdapter() {
        public void mousePressed(MouseEvent f) {
            if (f.getClickCount() == 1) {
                selectedName = contactsList.getSelectedValue();
            }
        }

        @Override
        public void mouseClicked(MouseEvent e) {
            if (e.getClickCount() == 2) {
                directMessageGUI = new DirectMessageGUI(username, selectedName, socket);
            }
        }
    };

    /**
     * when user selects a contact from the list of bookmarks the client calls the DirectMessage class
     **/
    public static MouseListener mouseListenerForBookmark = new MouseAdapter() {
        public void mousePressed(MouseEvent f) {
            if (f.getClickCount() == 1) {
                selectedName = bookmarkList.getSelectedValue();
            }
        }

        @Override
        public void mouseClicked(MouseEvent e) {
            if (e.getClickCount() == 2) {
                directMessageGUI = new DirectMessageGUI(username, selectedName, socket);
            }
        }
    };

    public static MouseListener mouseListenerForGroups = new MouseAdapter() {
        @Override
        public void mousePressed(MouseEvent e) {
            if (e.getClickCount() == 1) {
                selectedGroup = groupsList.getSelectedValue();
            }
        }

        public void mouseClicked(MouseEvent f) {
            if (f.getClickCount() == 2) {
                if (selectedGroup == null) {
                    return;
                } else {
                    try {
                        groupGUI = new GroupGUI(groupName, username, socket);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    };
}
