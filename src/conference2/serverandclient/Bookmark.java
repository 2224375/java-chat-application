package conference2.serverandclient;

import javax.swing.*;
import java.io.*;

/**
 * Used to mark a contact as favorite
 */
public class Bookmark {

    private BufferedReader reader;
    private String username;
    private String line;
    private String[] contactsDetails;
    private boolean toList;
    private boolean exists;
    boolean userFound;

    /**
     * Constructor for initializing instance variables
     * @param username username of the account
     */
    public Bookmark(String username) {
        this.username = username;
    }

    /**
     * Adds a selected user to be bookmarked.
     * Reads the details for the user's contact and updates it
     * @param name name of the account to be bookmarked
     * @throws FileNotFoundException
     */
    public void addToBookmark(String name) throws FileNotFoundException {
        BufferedReader bufferedReader = new BufferedReader(new FileReader("res/Users.txt"));
        BufferedReader contactDetailsReader = new BufferedReader(new FileReader("res/Users/" + username + "/Contacts_" + username + ".csv"));
        try {
            while ((line = bufferedReader.readLine()) != null) {
                this.contactsDetails = line.split(",");
                if (contactsDetails[0].equals(name)) {
                    userFound = true;
                    exists = true;
                }
            }
            StringBuffer sb = new StringBuffer();
            if (userFound != false) {
                while ((line = contactDetailsReader.readLine()) != null) {
                    this.contactsDetails = line.split(",");
                    if (contactsDetails[0].equals(name) && contactsDetails[2].equals("Not Bookmarked")) {
                        contactsDetails[2] = "Bookmarks";
                        toList = true;
                    }
                    sb.append(contactsDetails[0] + "," + contactsDetails[1] + "," + contactsDetails[2] + "\n");
                }
                String output = sb.toString();
                FileOutputStream fileOut = new FileOutputStream("res/Users/" + this.username + "/Contacts_" + this.username + ".csv");
                fileOut.write(output.getBytes());
                fileOut.close();
            } else {
                toList = false;
            }
        } catch (Exception ex) {
            exists = false;
            JOptionPane.showMessageDialog(null, "Username does not exist.");
        }
    }

    /**
     * Removes bookmark from database
     * @param name account to be removed from bookmarked
     * @throws IOException
     */
    public void removeBookmark(String name) throws IOException {
        this.reader = new BufferedReader(new FileReader("res/Users/" + this.username + "/Contacts_" + this.username + ".csv"));
        StringBuilder temp = new StringBuilder();

        while ((this.line = this.reader.readLine()) != null) {
            this.contactsDetails = line.split(",");
            if (contactsDetails[0].equals(name) && contactsDetails[2].equals("Bookmark")) {
                contactsDetails[2] = "Not Bookmark";
            }
            temp.append(contactsDetails[0] + "," + contactsDetails[1] + "," + contactsDetails[2] + "\n");
        }
        String output = temp.toString();
        FileOutputStream fileOut = new FileOutputStream("res/Users/" + this.username + "/Contacts_" + this.username + ".csv");
        fileOut.write(output.getBytes());
        fileOut.close();
    }

    /**
     * This method will proceed to the list of users
     * This was also used for verification
     * @return
     */

    public boolean continueToList() { return toList;}

    public boolean doesExist() {return exists;}
}
