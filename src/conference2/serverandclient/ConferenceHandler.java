package conference2.serverandclient;


import java.io.*;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

/**
 * Handles the group conference of the program
 */
class ConferenceHandler implements Runnable {

    private BufferedReader reader;
    private PrintWriter writer;
    private Socket socket;
    private String username;
    private String groupName;
    private String recipient;
    List<String> onlineUsers = new ArrayList<>();
    String chatStatus;
    private GroupContacts groupContacts;

    /**
     * Constructor for initializing instance variables
     * @param socket
     * @param username username of the account
     */
    public ConferenceHandler(Socket socket, String username) {
        this.socket = socket;
        this.username = username;
        onlineUsers.add(this.username);
    }

    /**
     * To read and write from the Direct message, Group message, and Broadcast
     */
    @Override
    public void run() {
        String message;
        try (
                BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
                PrintWriter writer = new PrintWriter(socket.getOutputStream(), true);
        ) {

            this.reader = reader;
            this.writer = writer;
            while (true) {
                message = reader.readLine();
                System.out.println("Message Server: " + message);
                if (message.equals("Group Message")) {
                    chatStatus = "Group Message";
                    String groupName = reader.readLine();
                    String username = reader.readLine();
                    String line = reader.readLine();

                    for (int i = 0; i >= onlineUsers.size(); i++){
                        onlineUsers.add(username);
                        addToAddedUsers(onlineUsers);
                    }
                    messageGroupHistory();
                    messageSent(message);
                } else if (message.equals("Private Message")) {
                    chatStatus = "Private Message";
                    String otherUserName = reader.readLine();
                    recipient = otherUserName;
                    messageHistory();
                    if (onlineUsers.size() == 1) {
                        onlineUsers.add(otherUserName);
                    } else if (onlineUsers.size() > 1) {
                        while (onlineUsers.size() > 1) {
                            onlineUsers.remove(1);
                        }
                        onlineUsers.add(otherUserName);
                    }
                    messageSent(message);
                } else if (message.equals("Broadcast")) {
                    messageBroadcastHistory();
                    chatStatus = "Broadcast";
                    String username = reader.readLine();
                    String line = reader.readLine();
                    for (ConferenceHandler client : ServerConference.onlineUsers) {
                        client.sendMessage(line);
                    }
                    onlineUsers.add(username);
                    messageSent(message);
                } else {
                    messageSent(" " + message);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * The method were utilized for sending messages whether its:
     * Direct message, group message, or in broadcast
     * @param msg message to be sent
     */

    private void messageSent(String msg) {
        if (chatStatus.equals("Private Message")) {
            sendToPrivate(msg);
        } else if (chatStatus.equals("Group Message")) {
            sendToGroup(msg);
        } else if (chatStatus.equals("Broadcast")) {
            sendToBroadcast(msg);
        }
    }

    /**
     * The method were used for writing the messages of the users from the direct message
     * into the private message history
     * @param msg message to be written into the private message history
     */
    private void writeToPrivateMessageLog(String msg) {
        try (
                Writer writeToUser = new BufferedWriter(new FileWriter("res/Users/" + username + "/Private Message_" + recipient + ".txt", true));
                Writer writeToRecipient = new BufferedWriter(new FileWriter("res/Users/" + recipient + "/Private Message_" + username + ".txt", true));
        ) {
            writeToUser.append("\n" + msg);
            writeToRecipient.append("\n" + msg);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * The method were utilized to read the message history of the user
     * The messages that are sent in the Direct Message
     */
    private void messageHistory() {
        try (
                BufferedReader reader = new BufferedReader(new FileReader("res/Users/" + username + "/Private Message_" + recipient + ".txt"));
        ) {
            String line;
            while ((line = reader.readLine()) != null) {
                sendMessage(line);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * The method were utilized to write the messages that were sent by the users from the broadcast
     * to the broadcast history
     * @param msg message to be written
     */
    private void writeBroadcastHistory(String msg) {
        try (
                Writer writer = new BufferedWriter(new FileWriter("res/Broadcast_Messages.txt", true));
        ) {
            writer.append("\n" + msg);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * The method were utilized for viewing/reading the broadcast history
     */
    private void messageBroadcastHistory() {
        try (
                BufferedReader reader = new BufferedReader(new FileReader("res/Broadcast_Messages.txt"));
        ) {
            String line;
            while ((line = reader.readLine()) != null) {
                sendMessage(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Writes the messages that were recorded from the Group conference
     * @param msg message to be written
     */
    private void writeGroupHistory(String msg) {
        try (BufferedReader reader = new BufferedReader(new FileReader("res/Groups.txt"))) {
            groupName = reader.readLine();
            Writer writer = new BufferedWriter(new FileWriter("res/Groups/" + groupName + "/Group Message_" + groupName + ".txt", true));
            writer.append("\n" + msg);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Accesses the group conference history
     */
    private void messageGroupHistory() {
        groupName = groupContacts.getGroupName();
        System.out.println(groupName);
        try (
                BufferedReader reader = new BufferedReader(new FileReader("res/Groups/" + groupName + "/Group Message_" + groupName + ".txt"))
        ) {
            String line;
            while ((line = reader.readLine()) != null) {
                sendMessage(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Broadcasts a message and may notify the users who are online
     * @param message message to be broadcast
     */
    private void sendToBroadcast(String message) {
        for (ConferenceHandler conferenceHandler : ServerConference.onlineUsers) {
            if (chatStatus.equals("Broadcast")) {
                conferenceHandler.sendMessage(message);
                writeBroadcastHistory(message);
                System.out.println(message);
            } else {
                writeBroadcastHistory(message);
                sendMessage(message);
            }
        }
    }

    /**
     * Sends a private message to the specified user
     * @param message message to be sent to the specified user
     */
    private void sendToPrivate(String message) {
        if (isUserOnline(recipient)) {
            if (chatStatus.equals("Private Message")) {
                ConferenceHandler clients = findUser(recipient);
                sendMessage(message);
                writeToPrivateMessageLog(message);
                clients.sendMessage(message);
            }
        } else {
            writeToPrivateMessageLog(message);
            sendMessage(message);
        }
    }

    /**
     * Sends a message to a group
     * Only the online users may view the message
     * @param message message to be sent
     */
    private void sendToGroup(String message) {
        for (String name : onlineUsers) {
            findUser(name).sendMessage(message);
            writeGroupHistory(message);
        }
    }

    /**
     * Checks if the user is online
     * @param username username of the account to be checked
     * @return if account if online or not
     */
    private boolean isUserOnline(String username) {
        for (ConferenceHandler client : ServerConference.onlineUsers) {
            if (client.getUsername().equals(username)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Searches users who are online
     * @param username username of the user to be searched
     * @return
     */
    public ConferenceHandler findUser(String username) {
        for (ConferenceHandler client : ServerConference.onlineUsers) {
            if (username.equals(client.getUsername())) {
                return client;
            }
        }
        return null;
    }

    /**
     * Adds users to the group conference
     * @param username account to be added to the group conference
     */
    public void addToAddedUsers(List<String> username) {
        for (ConferenceHandler client : ServerConference.onlineUsers) {
            if (!client.getUsername().equals(this.username)) {
                for (String name : username) {
                    if (!client.getUsername().equals(name)) {
                        client.chatStatus = "Group Message";
                        client.onlineUsers.add(name);
                    }
                }
            }
        }
    }

    private void sendMessage(String message) {
        writer.println(message);
    }

    /**
     * Returns the username of the account
     * @return username of the account
     */
    public String getUsername() {
        return username;
    }
}