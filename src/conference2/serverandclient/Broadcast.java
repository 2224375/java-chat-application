package conference2.serverandclient;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Broadcast extends conference2.gui.Broadcast {

    /**
     * Constructor for initializing instance variables
     *
     * @param username username of the account
     * @param socket
     * @throws IOException
     */
    public Broadcast(String username, Socket socket) throws IOException {
        super(username, socket);
    }

    public static class ButtonHandler implements ActionListener {
        /**
         * Called when the exitButton or sendButton is pressed
         * @param e the event to be processed
         */
        public void actionPerformed(ActionEvent e) {
            if (e.getSource() == exitButton) {
                window.dispose();

            } else if (e.getSource() == sendButton) {
                if (textInput.getText().equals("")) {
                    sendButton.disable();
                } else if (textInput.getText() != null) {
                    if (e.getSource() == sendButton) {
                        String msg = textInput.getText();
                        send(msg);
                        textInput.setText("");
                        sendButton.disable();
                    }
                }
            }
        }
    }

    public static class MessagesThread extends Thread {
        @Override
        public void run() {
            String line;
            try {
                BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
                PrintWriter writer = new PrintWriter(socket.getOutputStream(), true);
                bufferedReader = reader;
                printWriter = writer;
                while (true) {
                    line = reader.readLine();
                    if (line.equals("Broadcast History Message")) {
                       String msg = reader.readLine();
                       System.out.println(msg);
                       broadcastMessages.append("\n" + msg);
                        broadcastMessages.setCaretPosition(broadcastMessages.getDocument().getLength());//auto scroll to last message
                   } else {
                        System.out.println("Broadcast: " + line);
                        broadcastMessages.append("\n" + line);
                        broadcastMessages.setCaretPosition(broadcastMessages.getDocument().getLength());
                    }
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }

    /**
     * Sends a message to the server
     * @param msg message to be sent
     */
    private static void send(String msg) {
        SimpleDateFormat timeStamp = new SimpleDateFormat("EEE| h:mm a");
        try {
            String[] messages = msg.split(" ");
            PrintWriter writer = new PrintWriter(socket.getOutputStream(), true);
            if (messages[0].equals("Broadcast")) {
                writer.println("Broadcast");
                writer.println(timeStamp.format(new Date()) + "| From " + username + ":\n" + msg);
            } else {
                writer.println(timeStamp.format(new Date()) + "| From " + username + ":\n" + msg);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Sends a message to the server
     * @param message message to be sent
     */
    public static void sendMessageToServer(String message) {
        try {
            PrintWriter writer = new PrintWriter(socket.getOutputStream(), true);
            writer.println(message);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
