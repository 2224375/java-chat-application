package conference2.gui;

import conference2.serverandclient.CreateUser;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;
import java.net.Socket;

/**
 * Creates a page for logging in and creating a user
 */
public class CreateGUI extends JFrame {

    public static final JFrame window = new JFrame();
    public static final JLabel messenger = new JLabel();
    public static final JPanel loginPanel = new JPanel();
    public static final JButton createButton1 = new JButton();
    public static final  JButton cancelButton = new JButton();
    public static final JButton loginButton = new JButton();
    public static final JButton createButton = new JButton();
    public static final JTextField userCreTextBox = new JTextField();
    public static final JPasswordField passCreTextBox = new JPasswordField();
    public static final JPasswordField confirmPasswordTextBox = new JPasswordField();
    public static final JTextField usernameTextBox = new JTextField();
    public static final JPasswordField passwordTextBox = new JPasswordField();
    public static final JLabel userCreLabel = new JLabel();
    public static final JLabel passCreLabel = new JLabel();
    public static final JLabel confirmPassLabel = new JLabel();
    public static final JLabel passLabel = new JLabel();
    public static final JLabel userLabel = new JLabel();
    public static Socket socket;
    private ActionListener creButtonHandler = new CreateUser.creButtonHandler();
    private ActionListener buttonHandler = new CreateUser.ButtonHandler();

    /** Creates the login and create user interface **/
    public CreateGUI(Socket socket) {

        this.socket = socket;
        window.setTitle("Login");
        window.setSize(800, 400);
        window.setVisible(true);
        window.getContentPane().setBackground(new Color(128,128,128));
        window.setLayout(null);
        window.setResizable(false);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        window.setLocationRelativeTo(null);

        window.add(messenger);
        window.add(userLabel);
        window.add(usernameTextBox);
        window.add(passLabel);
        window.add(passwordTextBox);
        window.add(createButton);
        window.add(loginButton);
        window.add(loginPanel);
        window.add(userCreLabel);
        window.add(userCreTextBox);
        window.add(passCreLabel);
        window.add(passCreTextBox);
        window.add(confirmPassLabel);
        window.add(confirmPasswordTextBox);
        window.add(createButton1);
        window.add(cancelButton);

        messenger.setText("Log in/Create");
        messenger.setFont(new Font("Helvetica", Font.BOLD, 25));
        messenger.setForeground(new Color(0,0,54));
        messenger.setSize(190, 30);
        messenger.setLocation(320, 25);

        userLabel.setText("Username:");
        userLabel.setFont(new Font("Helvetica", Font.BOLD, 15));
        userLabel.setForeground(new Color(0,0,54));
        userLabel.setSize(250, 20);
        userLabel.setLocation(360, 70);

        usernameTextBox.setSize(245, 25);
        usernameTextBox.setLocation(270, 95);

        passLabel.setText("Password:");
        passLabel.setFont(new Font("Helvetica", Font.BOLD, 15));
        passLabel.setForeground(new Color(0,0,54));
        passLabel.setSize(110, 20);
        passLabel.setLocation(360, 140);

        passwordTextBox.setSize(245, 25);
        passwordTextBox.setLocation(270, 170);

        userCreLabel.setText("Username:");
        userCreLabel.setFont(new Font("Helvetica", Font.BOLD, 15));
        userCreLabel.setForeground(new Color(0,0,54));
        userCreLabel.setSize(250, 20);
        userCreLabel.setLocation(360, 30);

        userCreTextBox.setSize(245, 25);
        userCreTextBox.setLocation(270, 60);

        passCreLabel.setText("Password: ");
        passCreLabel.setFont(new Font("Helvetica", Font.BOLD, 15));
        passCreLabel.setForeground(new Color(0,0,54));
        passCreLabel.setSize(100, 20);
        passCreLabel.setLocation(360, 100);

        passCreTextBox.setSize(245, 25);
        passCreTextBox.setLocation(270, 130);

        confirmPassLabel.setText("Confirm Password: ");
        confirmPassLabel.setFont(new Font("Helvetica", Font.BOLD, 11));
        confirmPassLabel.setForeground(new Color(0,0,54));
        confirmPassLabel.setSize(120, 20);
        confirmPassLabel.setLocation(340, 170);

        confirmPasswordTextBox.setSize(245, 25);
        confirmPasswordTextBox.setLocation(270, 200);

        loginButton.setText("Login");
        loginButton.setBorderPainted(false);
        loginButton.setBackground(new Color(128,128,128));
        loginButton.setFont(new Font("Helvetica", Font.BOLD, 12));
        loginButton.setForeground(new Color(0,0,54));
        loginButton.setSize(175, 35);
        loginButton.setLocation(200, 260);
        loginButton.setFocusPainted(false);

        createButton.setText("Create");
        createButton.setBorderPainted(false);
        createButton.setBackground(new Color(128,128,128));
        createButton.setFont(new Font("Helvetica", Font.BOLD, 12));
        createButton.setForeground(new Color(0,0,54));
        createButton.setSize(175, 35);
        createButton.setLocation(400, 260);
        createButton.setFocusPainted(false);

        createButton1.setText("Create");
        createButton1.setBorderPainted(false);
        createButton1.setBackground(new Color(128,128,128));
        createButton1.setFont(new Font("Helvetica", Font.BOLD, 12));
        createButton1.setForeground(new Color(0,0,54));
        createButton1.setSize(170, 30);
        createButton1.setLocation(230, 270);
        createButton1.setFocusPainted(false);

        cancelButton.setText("Cancel");
        cancelButton.setBorderPainted(false);
        cancelButton.setBackground(new Color(128,128,128));
        cancelButton.setFont(new Font("Helvetica", Font.BOLD, 12));
        cancelButton.setForeground(new Color(0,0,54));
        cancelButton.setSize(170, 30);
        cancelButton.setLocation(410, 270);
        cancelButton.setFocusPainted(false);

        loginButton.addActionListener(buttonHandler);
        createButton.addActionListener(buttonHandler);
        cancelButton.addActionListener(creButtonHandler);
        createButton1.addActionListener(creButtonHandler);

        userCreLabel.setVisible(false);
        userCreTextBox.setVisible(false);
        passCreLabel.setVisible(false);
        passCreTextBox.setVisible(false);
        confirmPassLabel.setVisible(false);
        confirmPasswordTextBox.setVisible(false);
        createButton1.setVisible(false);
        cancelButton.setVisible(false);
    }

}