package conference2.gui;

import conference2.serverandclient.DirectMessage;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.PrintWriter;
import java.net.Socket;


public class DirectMessageGUI {

    public static JFrame privateMessageWindow;
    public static BufferedReader bufferedReader;
    public static PrintWriter printWriter;
    public static JPanel panelForPrivateMessage = new JPanel();
    public static JTextArea privateMessageTextArea = new JTextArea();
    public static JTextField privateMessageTextBox = new JTextField();
    public static JButton sendButton = new JButton("Send");
    public static String recipient;
    public static String username;
    public static Socket socket;

    //public static PrivateMessageModel sendMessage;
    private ActionListener buttonhandler = new DirectMessage.ButtonHandler();

    /** create an interface for private messages **/
    public DirectMessageGUI(String username, String recipient, Socket socket) {
        this.username = username;
        this.socket = socket;
        this.recipient = recipient;

        Thread privateMessageThread = new Thread(new DirectMessage.MessagesThread());
        privateMessageThread.start();
        privateMessageWindow = new JFrame("Now chatting to " + recipient);
        privateMessageWindow.setSize(395, 600);
        privateMessageWindow.setVisible(true);
        privateMessageWindow.getContentPane().setBackground(new Color(128,128,128));
        privateMessageWindow.setLayout(null);
        privateMessageWindow.setResizable(false);
        privateMessageWindow.setLocationRelativeTo(null);

        privateMessageWindow.add(panelForPrivateMessage);
        privateMessageWindow.add(privateMessageTextArea);
        privateMessageWindow.add(sendButton);
        privateMessageWindow.add(privateMessageTextBox);

        privateMessageTextArea.selectAll();
        privateMessageTextArea.replaceSelection("");
        privateMessageTextArea.setFont(new Font("Helvetica", Font.PLAIN, 13));
        privateMessageTextArea.setSize(360, 500);
        privateMessageTextArea.setLocation(10, 10);
        privateMessageTextArea.setEditable(false);
        privateMessageTextArea.setText(" ");

        privateMessageTextBox.setSize(280, 30);
        privateMessageTextBox.setLocation(10, 520);

        sendButton.setSize(70, 30);
        sendButton.setBorderPainted(false);
        sendButton.setBackground(new Color(128,128,128));
        sendButton.setFont(new Font("Helvetica", Font.BOLD, 12));
        sendButton.setForeground(new Color(0,0,54));
        sendButton.setLocation(300, 520);
        sendButton.setFocusPainted(false);

        JScrollPane privateMessageScroller = new JScrollPane(privateMessageTextArea);
        privateMessageScroller.setBounds(10, 10, 360, 500);
        privateMessageWindow.add(privateMessageScroller);
        sendButton.addActionListener(buttonhandler);
        DirectMessage.sendMessageToServer("Private Message" + "\n" + recipient);
    }
}
