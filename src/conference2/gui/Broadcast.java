package conference2.gui;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

/**
 * Creates a GUI for the broadcast feature
 */
public class Broadcast {

    public static JFrame window = new JFrame("Broadcasting to Everyone");
    public static JTextArea broadcastMessages;
    public static JTextField textInput;
    public static JButton sendButton;
    public static JButton exitButton;
    public static PrintWriter printWriter;
    public static BufferedReader bufferedReader;
    public static String username;
    public static Socket socket;
    public conference2.serverandclient.Broadcast sendMessageToServer;
    private ActionListener buttonHandler = new conference2.serverandclient.Broadcast.ButtonHandler();

    DefaultListModel<String> userList = new DefaultListModel();
    JList<String> usersList = new JList<>(userList);

    /**
     * Constructor for initializing instance variables
     * @param username
     * @param socket
     * @throws IOException
     */
    public Broadcast(String username, Socket socket) throws IOException {

        window.setTitle("Connected as: " + username);  // set title for frame
        this.username = username;
        this.socket = socket;
        new conference2.serverandclient.Broadcast.MessagesThread().start();
        sendButton = new JButton("Send");
        sendButton.setBorderPainted(false);
        sendButton.setBackground(new Color(128, 128, 128));
        sendButton.setFont(new Font("Helvetica", Font.BOLD, 12));
        sendButton.setForeground(new Color(0,0,54));
        sendButton.setFocusPainted(false);

        exitButton = new JButton("Exit");
        exitButton.setBorderPainted(false);
        exitButton.setBackground(new Color(128,128,128));
        exitButton.setFont(new Font("Helvetica", Font.BOLD, 12));
        exitButton.setForeground(new Color(0,0,54));
        exitButton.setSize(120, 25);
        exitButton.setFocusPainted(false);

        broadcastMessages = new JTextArea();
        broadcastMessages.selectAll();
        broadcastMessages.replaceSelection("");
        broadcastMessages.setFont(new Font("Helvetica", Font.PLAIN, 13));
        broadcastMessages.setSize(360, 500);
        broadcastMessages.setRows(10);
        broadcastMessages.setColumns(50);
        broadcastMessages.setEditable(false);
        broadcastMessages.setText("");

        usersList.setSize(230, 440);
        usersList.setLocation(20, 80);
        usersList.setFont(new Font("Helvetica", Font.PLAIN, 13));
        usersList.setVisible(true);


        JScrollPane chatPanel = new JScrollPane(broadcastMessages, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
                JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
        JScrollPane onlineUsersPanel = new JScrollPane(usersList, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
                JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);

        JPanel topPanel = new JPanel(new FlowLayout());
        topPanel.add(chatPanel);
        topPanel.add(onlineUsersPanel);
        window.add(topPanel, "Center");

        textInput = new JTextField(50);

        JPanel bottomPanel = new JPanel(new FlowLayout());
        bottomPanel.add(textInput);
        bottomPanel.add(sendButton);
        bottomPanel.add(exitButton);
        window.add(bottomPanel, "South");
        sendButton.addActionListener(buttonHandler);
        exitButton.addActionListener(buttonHandler);
        window.setSize(500, 300);
        window.setVisible(true);
        window.setResizable(false);
        window.pack();

        bufferedReader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        sendMessageToServer.sendMessageToServer("Broadcast" + "\n");

    }
}
