package conference2.gui;


import conference2.serverandclient.GroupContacts;
import conference2.serverandclient.Messenger;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.net.Socket;
import java.util.ArrayList;

public class MessengerGUI extends JFrame {

    public static JFrame generalWindow = new JFrame("Messenger");
    public static JLabel userAccount = new JLabel();
    public static JButton broadcast = new JButton("Broadcast");
    public static JLabel contactsLabel = new JLabel("Message Contacts");
    public static JLabel groupsLabel = new JLabel("Message Groups");
    public static JLabel bookmarkLabel = new JLabel("Message Bookmark");
    public static JButton addContact = new JButton("Add Contacts");
    public static JButton addGroup = new JButton("Add Group");
    public static JButton addBookmark = new JButton("Add Bookmark");
    public static JButton searchContactOrGroup = new JButton("Search Contact/Group");
    public static JButton showContacts = new JButton("Show Contacts");
    public static JButton showGroups = new JButton("Show Groups");
    public static JButton showBookmark = new JButton("Show Bookmarks");
    public static JButton removeContact = new JButton("Remove Contact");
    public static JButton removeGroup = new JButton("Remove Group");
    public static JButton removeBookmark = new JButton("Remove Bookmark");
    public static DirectMessageGUI directMessageGUI;
    public static GroupGUI groupGUI;
    public static Socket socket;
    public static String username;
    public static String selectedName;
    public static String selectedGroup;
    public static JScrollPane contactsListScroller;
    public static JScrollPane groupsListScroller;
    public static JScrollPane bookmarkListScoller;
    public static GroupContacts newGroup;
    public static String groupName;
    public static ArrayList<String> membersArray;


    public static DefaultListModel<String> contactList = new DefaultListModel();
    public static JList<String> contactsList = new JList<>(contactList);
    public static DefaultListModel<String> groupList = new DefaultListModel();
    public static JList<String> groupsList = new JList<>(groupList);
    public static DefaultListModel<String> bookmarklist = new DefaultListModel();
    public static JList<String> bookmarkList = new JList<>(bookmarklist);
    private ActionListener button = new Messenger.ButtonHandler();


    /**
     * creates the main interface for the application
     **/
    public MessengerGUI(Socket socket, String username) {
        this.socket = socket;
        this.username = username;
        SwingUtilities.invokeLater(() -> {
            try (BufferedReader reader = new BufferedReader(new FileReader("res/Users/" + username + "/Contacts_" + username + ".csv"));
//                 BufferedReader bufferedReader = new BufferedReader(new FileReader("res/Groups.txt"))
            ) {
                String line;
                String[] textSplit;
                while ((line = reader.readLine()) != null) {
                    textSplit = line.split(",");
                    String name = textSplit[0];
                    contactList.addElement(name);
                    if (textSplit[1].equals("Group")) {
                        groupList.addElement(name);
                    }
                    if (textSplit[2].equals("Bookmark")) {
                        bookmarklist.addElement(name);
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            }

            generalWindow.setSize(390, 725);
            generalWindow.setVisible(true);
            generalWindow.getContentPane().setBackground(new Color(128,128,128));
            generalWindow.setLayout(null);
            generalWindow.setResizable(false);
            generalWindow.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            generalWindow.setLocationRelativeTo(null);

            generalWindow.add(userAccount);
            generalWindow.add(broadcast);
            generalWindow.add(contactsLabel);
            generalWindow.add(groupsLabel);
            generalWindow.add(bookmarkLabel);
            generalWindow.add(contactsList);
            generalWindow.add(groupsList);
            generalWindow.add(bookmarkList);
            generalWindow.add(addContact);
            generalWindow.add(addGroup);
            generalWindow.add(addBookmark);
            generalWindow.add(searchContactOrGroup);
            generalWindow.add(showContacts);
            generalWindow.add(showGroups);
            generalWindow.add(showBookmark);
            generalWindow.add(removeContact);
            generalWindow.add(removeGroup);
            generalWindow.add(removeBookmark);

            userAccount.setText("Welcome, " + username + "!");
            userAccount.setSize(180, 25);
            userAccount.setLocation(120, 20);
            userAccount.setFont(new Font("Helvetica", Font.BOLD, 20));
            userAccount.setForeground(new Color(0,0,54));


            broadcast.setSize(110, 25);
            broadcast.setBorderPainted(false);
            broadcast.setBackground(new Color(128,128,128));
            broadcast.setLocation(225, 570);
            broadcast.setFont(new Font("Helvetica", Font.BOLD, 10));
            broadcast.setForeground(new Color(0,0,54));
            broadcast.setFocusPainted(false);

            contactsLabel.setSize(150, 40);
            contactsLabel.setLocation(140, 40);
            contactsLabel.setFont(new Font("Helvetica", Font.BOLD, 12));
            contactsLabel.setForeground(new Color(0,0,54));
            contactsLabel.setVisible(true);

            groupsLabel.setSize(140, 40);
            groupsLabel.setLocation(140, 40);
            groupsLabel.setFont(new Font("Helvetica", Font.BOLD, 12));
            groupsLabel.setForeground(new Color(0,0,54));
            groupsLabel.setVisible(false);

            bookmarkLabel.setSize(140, 40);
            bookmarkLabel.setLocation(140, 40);
            bookmarkLabel.setFont(new Font("Helvetica", Font.BOLD, 12));
            bookmarkLabel.setForeground(new Color(0,0,54));
            bookmarkLabel.setVisible(false);

            contactsList.setSize(230, 440);
            contactsList.setLocation(20, 80);
            contactsList.setFont(new Font("Helvetica", Font.PLAIN, 13));
            contactsList.addMouseListener(Messenger.mouseListenerForContacts);
            contactsList.setVisible(true);

            groupsList.setSize(230, 440);
            groupsList.setLocation(20, 80);
            groupsList.setFont(new Font("Helvetica", Font.PLAIN, 13));
            groupsList.addMouseListener(Messenger.mouseListenerForGroups);
            groupsList.setVisible(false);


            bookmarkList.setSize(230, 440);
            bookmarkList.setLocation(20, 80);
            bookmarkList.setFont(new Font("Helvetica", Font.PLAIN, 13));
            bookmarkList.addMouseListener(Messenger.mouseListenerForBookmark);
            bookmarkList.setVisible(false);

            contactsListScroller = new JScrollPane(contactsList);
            contactsListScroller.setBounds(20, 80, 345, 440);
            groupsListScroller = new JScrollPane(groupsList);
            groupsListScroller.setBounds(20, 80, 345, 440);
            bookmarkListScoller = new JScrollPane(bookmarkList);
            bookmarkListScoller.setBounds(20, 80, 345, 440);

            generalWindow.add(contactsListScroller);
            generalWindow.add(groupsListScroller);
            generalWindow.add(bookmarkListScoller);

            addContact.setSize(110, 30);
            addContact.setBorderPainted(false);
            addContact.setBackground(new Color(128,128,128));
            addContact.setLocation(60, 530);
            addContact.setFont(new Font("Helvetica", Font.BOLD, 10));
            addContact.setForeground(new Color(0,0,54));
            addContact.setVisible(true);
            addContact.setFocusPainted(false);

            addGroup.setSize(110, 30);
            addGroup.setBorderPainted(false);
            addGroup.setBackground(new Color(128,128,128));
            addGroup.setLocation(60, 530);
            addGroup.setFont(new Font("Helvetica", Font.BOLD, 10));
            addGroup.setForeground(new Color(0,0,54));
            addGroup.setVisible(false);
            addGroup.setFocusPainted(false);

            addBookmark.setSize(110, 30);
            addBookmark.setBorderPainted(false);
            addBookmark.setBackground(new Color(128,128,128));
            addBookmark.setLocation(60, 530);
            addBookmark.setFont(new Font("Helvetica", Font.BOLD, 10));
            addBookmark.setForeground(new Color(0,0,54));
            addBookmark.setVisible(false);
            addBookmark.setFocusPainted(false);

            showContacts.setSize(110, 30);
            showContacts.setBorderPainted(false);
            showContacts.setBackground(new Color(128,128,128));
            showContacts.setLocation(225, 530);
            showContacts.setFont(new Font("Helvetica", Font.BOLD, 10));
            showContacts.setForeground(new Color(0,0,54));
            showContacts.setVisible(false);
            showContacts.setFocusPainted(false);

            showGroups.setSize(110, 30);
            showGroups.setBorderPainted(false);
            showGroups.setBackground(new Color(128,128,128));
            showGroups.setLocation(225, 530);
            showGroups.setFont(new Font("Helvetica", Font.BOLD, 10));
            showGroups.setForeground(new Color(0,0,54));
            showGroups.setVisible(true);
            showGroups.setFocusPainted(false);

            searchContactOrGroup.setSize(110, 25);
            searchContactOrGroup.setBorderPainted(false);
            searchContactOrGroup.setBackground(new Color(128,128,128));
            searchContactOrGroup.setLocation(60, 570);
            searchContactOrGroup.setFont(new Font("Helvetica", Font.BOLD, 10));
            searchContactOrGroup.setForeground(new Color(0,0,54));
            searchContactOrGroup.setFocusPainted(false);

            showBookmark.setSize(110, 25);
            showBookmark.setBorderPainted(false);
            showBookmark.setBackground(new Color(128,128,128));
            showBookmark.setLocation(60, 610);
            showBookmark.setFont(new Font("Helvetica", Font.BOLD, 10));
            showBookmark.setForeground(new Color(0,0,54));
            showBookmark.setFocusPainted(false);

            removeContact.setSize(110, 25);
            removeContact.setBorderPainted(false);
            removeContact.setBackground(new Color(128,128,128));
            removeContact.setLocation(225, 610);
            removeContact.setFont(new Font("Helvetica", Font.BOLD, 10));
            removeContact.setForeground(new Color(0,0,54));
            removeContact.setVisible(true);
            removeContact.setFocusPainted(false);

            removeGroup.setSize(110, 25);
            removeGroup.setBorderPainted(false);
            removeGroup.setBackground(new Color(128,128,128));
            removeGroup.setLocation(225, 610);
            removeGroup.setFont(new Font("Helvetica", Font.BOLD, 10));
            removeGroup.setForeground(new Color(0,0,54));
            removeGroup.setVisible(false);
            removeGroup.setFocusPainted(false);

            removeBookmark.setSize(110, 25);
            removeBookmark.setBorderPainted(false);
            removeBookmark.setBackground(new Color(128,128,128));
            removeBookmark.setLocation(225, 610);
            removeBookmark.setFont(new Font("Helvetica", Font.BOLD, 10));
            removeBookmark.setForeground(new Color(0,0,54));
            removeBookmark.setVisible(false);
            removeBookmark.setFocusPainted(false);

            broadcast.addActionListener(button);
            addContact.addActionListener(button);
            addGroup.addActionListener(button);
            addBookmark.addActionListener(button);
            searchContactOrGroup.addActionListener(button);
            showContacts.addActionListener(button);
            showGroups.addActionListener(button);
            showBookmark.addActionListener(button);
            removeContact.addActionListener(button);
            removeGroup.addActionListener(button);
            removeBookmark.addActionListener(button);

            generalWindow.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            generalWindow.setVisible(true);

        });
    }
}
