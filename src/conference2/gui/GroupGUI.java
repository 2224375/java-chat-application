package conference2.gui;

import conference2.serverandclient.GroupConference;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

public class GroupGUI {

    public static JFrame groupConferenceWindow = new JFrame();
    public static  BufferedReader bufferedReader;
    public static  PrintWriter printWriter;
    public static JLabel groupMembersLabel = new JLabel("Group Members");
    public static JTextArea groupChat;
    public static JTextField messageBox;
    public static  JButton sendButton = new JButton("Send");
    public static JButton removeButton = new JButton("Kick Member");
    public static JButton addMember = new JButton("Add Members");
    public static Socket socket;
    public static String user;
    public static String groupName;
    public static String memberName;
    public static String selectedName;

    private ActionListener buttons = new GroupConference.ButtonHandler();

    public static final DefaultListModel<String> groupMembers = new DefaultListModel();
    public static final JList<String> groupMembersList = new JList<>(groupMembers);

    public GroupGUI(String groupName, String user, Socket socket) throws IOException {
        this.groupName = groupName;
        this.user = user;
        this.socket = socket;
        Thread groupConferenceThread = new Thread(new GroupConference.MessagesThread());
        groupConferenceThread.start();
        groupConferenceWindow.setSize(540, 600);
        groupConferenceWindow.setTitle(groupName);
        groupConferenceWindow.setVisible(true);
        groupConferenceWindow.getContentPane().setBackground(new Color(128,128,128));
        groupConferenceWindow.setLayout(null);
        groupConferenceWindow.setResizable(true);
        groupConferenceWindow.setLocationRelativeTo(null);

        groupChat = new JTextArea();
        groupChat.selectAll();
        groupChat.replaceSelection("");
        groupChat.setFont(new Font("Helvetica", Font.PLAIN, 13));
        groupChat.setSize(350, 400);
        groupChat.setLocation(10, 10);
        groupChat.setEditable(false);
        groupChat.setVisible(true);
        groupChat.setText("");

        messageBox = new JTextField();
        messageBox.setSize(280, 30);
        messageBox.setLocation(10, 520);
        messageBox.setVisible(true);

        groupMembersList.setSize(135, 350);
        groupMembersList.setLocation(380, 30);
        groupMembersList.setFont(new Font("Helvetica", Font.PLAIN, 13));
        groupMembersList.addMouseListener(GroupConference.mouseListenerForGroupMembers);
        groupMembersList.setVisible(true);

        groupMembersLabel.setSize(200, 20);
        groupMembersLabel.setLocation(380, 10);
        groupMembersLabel.setFont(new Font("Helvetica", Font.BOLD, 11));
        groupMembersLabel.setForeground(new Color(0,0,54));
        groupMembersLabel.setVisible(true);

        groupConferenceWindow.add(groupMembersLabel);
        groupConferenceWindow.add(groupMembersList);

        sendButton.setSize(70, 30);
        sendButton.setBorderPainted(false);
        sendButton.setBackground(new Color(128,128,128));
        sendButton.setFont(new Font("Helvetica", Font.BOLD, 12));
        sendButton.setForeground(new Color(0,0,54));
        sendButton.setLocation(300, 520);
        sendButton.setVisible(true);
        sendButton.setFocusPainted(false);

        addMember.setSize(135, 30);
        addMember.setBorderPainted(false);
        addMember.setBackground(new Color(128,128,128));
        addMember.setFont(new Font("Helvetica", Font.BOLD, 12));
        addMember.setForeground(new Color(0,0,54));
        addMember.setLocation(380, 390);
        addMember.setVisible(true);
        addMember.setFocusPainted(false);

        removeButton.setSize(135, 30);
        removeButton.setBorderPainted(false);
        removeButton.setBackground(new Color(128,128,128));
        removeButton.setFont(new Font("Helvetica", Font.BOLD, 12));
        removeButton.setForeground(new Color(0,0,54));
        removeButton.setLocation(380, 430);
        removeButton.setVisible(true);
        removeButton.setFocusPainted(false);

        groupConferenceWindow.add(groupChat);
        groupConferenceWindow.add(messageBox);
        groupConferenceWindow.add(sendButton);
        groupConferenceWindow.add(addMember);
        groupConferenceWindow.add(removeButton);

        JScrollPane privateMessageScroller = new JScrollPane(groupChat);
        privateMessageScroller.setBounds(10, 10, 360, 500);
        groupConferenceWindow.add(privateMessageScroller);

        sendButton.addActionListener(buttons);
        addMember.addActionListener(buttons);
        removeButton.addMouseListener(GroupConference.mouseListenerForGroupMembers);


        bufferedReader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        GroupConference.sendMessageToServer("Group Message" + "\n");

    }
}